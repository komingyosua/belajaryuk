package com.app.belajaryuk.Global;

public class GlobalIndex {
  private static GlobalIndex instance = null;

  public static int indexEvent = 0;
  public static int indexVolunteer = 0;
  public static int indexDonation = 0;

  public static int indexHistoryDonation = 0;
  public static int indexHistoryVolunteer = 0;
  public static int totalHistoryActivity = 0;

  public static int indexFavoriteEvent = 0;
  public static int indexFavoriteVolunteer = 0;
  public static int indexFavoriteDonation = 0;

  public static int indexPersonal = 0;
  public static int indexCommunity = 0;



  public static synchronized GlobalIndex getInstance(){
      if(null == instance){
        instance = new GlobalIndex();
      }
      return instance;

  }
}
