package com.app.belajaryuk.Global;

import android.content.Context;

import com.app.belajaryuk.Data.Event;

import java.util.ArrayList;
import java.util.List;

public class EventGlobalClass {

    private static Context context;
    private static ArrayList<Event> events;

    public EventGlobalClass(Context context) {
        EventGlobalClass.context = context;
        events = new ArrayList<>();
    }

    public static List<Event> getList() {
        return events;
    }

    public static void add(Event items) {
        events.add(items);
    }

}
