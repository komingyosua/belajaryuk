package com.app.belajaryuk;

import android.app.Application;

public class IntroApp extends Application {
    private static IntroApp instance;

    public static IntroApp getInstance() {
        return instance;
    }

    public static IntroApp getContext(){
        return instance;
        // or return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        PrefManager.init(this);
    }



}
