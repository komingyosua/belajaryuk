package com.app.belajaryuk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.app.belajaryuk.Community.Activity.Community;
import com.app.belajaryuk.Personal.Activity.Personal;
import com.app.belajaryuk.Utils.SharedPrefManager;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    Button btnGetStarted, btnNext;
    TextView btnBack,skip;
    SharedPrefManager sharedPrefManager;

    private int[] layouts = new int[]{R.layout.slide1, R.layout.slide2, R.layout.slide3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefManager = new SharedPrefManager(WelcomeActivity.this);

        if (sharedPrefManager.getSPIsLogin() && sharedPrefManager.getSPRole().equals("personal")){
            startActivity(new Intent(WelcomeActivity.this, Personal.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }else if(sharedPrefManager.getSPIsLogin() && sharedPrefManager.getSPRole().equals("community")){
            startActivity(new Intent(WelcomeActivity.this, Community.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        if (!PrefManager.shouldShowSlider()) {
            launchHomeScreen();
            finish();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        setContentView(R.layout.activity_welcome);

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        btnGetStarted = findViewById(R.id.btn_getStarted);
        btnNext = findViewById(R.id.btn_next);
        btnBack = findViewById(R.id.btn_back);
        skip = findViewById(R.id.tv_skip);
        btnGetStarted.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        skip.setOnClickListener(this);

//        addBottomDots();
//        updateBottomDots(0, 0);



        viewPager.setAdapter(new MyViewPagerAdapter());
        viewPager.addOnPageChangeListener(pageChangeListener);

    }

    private void showHideView(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private void launchHomeScreen() {
        PrefManager.saveFirstTimeLaunch(true);
        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        finish();
    }

//    private void addBottomDots() {
//        if ((dotsLayout == null) || (layouts == null))
//            return;
//            return;
//
//        int dotSize = layouts.length;
//        dotsLayout.removeAllViews();
//
//        dots = new TextView[dotSize];
//        for (int i = 0; i < dots.length; i++) {
//            dots[i] = new TextView(this);
//            dots[i].setText(Html.fromHtml("&#8226;"));
//            dots[i].setTextSize(35);
//            dotsLayout.addView(dots[i]);
//        }
//    }

    private void updateBottomDots(int prevPosition, int curPosition) {
        if (dots == null)
            return;

        int dotLength = dots.length;
        if ((dotLength > prevPosition) && (dotLength > curPosition)) {
            dots[prevPosition].setTextColor(getResources().getColor(R.color.dot_inactive));
            dots[curPosition].setTextColor(getResources().getColor(R.color.dot_active));
        }
    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        int prevPos = 0;

        @Override
        public void onPageSelected(int position) {
            updateBottomDots(prevPos, position);
            boolean isLastPage = (position == (layouts.length - 1));
            boolean isFirstPage = (position == 0);
            boolean isMiddle = (position == 1);
            showHideView(R.id.btn_back, isMiddle ? View.VISIBLE : View.GONE);
            showHideView(R.id.btn_next, isLastPage ? View.GONE : View.VISIBLE);
            showHideView(R.id.tv_skip, isLastPage ? View.GONE : View.VISIBLE);
            showHideView(R.id.btn_getStarted, isLastPage ? View.VISIBLE : View.GONE);
            if(isLastPage){
                YoYo.with(Techniques.BounceInUp).duration(1000).playOn(btnGetStarted);
            }

            prevPos = position;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_next:
                showNextSlide();
                break;
            case R.id.btn_back:
                showPrevSlide();
                break;
            case R.id.tv_skip:
            case R.id.btn_getStarted:
                launchHomeScreen();
                break;
        }
    }

    private void showNextSlide() {
        int nextIndex = viewPager.getCurrentItem() + 1;
        if ((viewPager != null) && (nextIndex < layouts.length)) {
            viewPager.setCurrentItem(nextIndex);
        }
    }
    private void showPrevSlide() {
        int prevIndex = viewPager.getCurrentItem() - 1;
        if ((viewPager != null) && (prevIndex < layouts.length)) {
            viewPager.setCurrentItem(prevIndex);
        }
    }



    private class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }



        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }


        @Override
        public int getCount() {
            return (layouts != null) ? layouts.length : 0;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

    }
}
