package com.app.belajaryuk.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    public static final String SP_LOGIN_APP = "sp_login_app";

    public static final String SP_NAME = "sp_name";
    public static final String SP_EMAIL = "sp_email";
    public static final String SP_TOKEN = "sp_token";
    public static final String SP_ROLE = "sp_role";
    public static final String SP_UPLOADED_EVENT = "sp_uploaded_event";
    public static final String SP_UPLOADED_VOLUNTEER = "sp_uploaded_volunteer";

    public static final String SP_IS_LOGIN = "sp_is_login";

    SharedPreferences sp;
    SharedPreferences.Editor sp_editor;


    public SharedPrefManager(Context context){
        sp = context.getSharedPreferences(SP_LOGIN_APP, Context.MODE_PRIVATE);
        sp_editor = sp.edit();
    }

    public int getCount() {
        int count = sp.getInt("count", -1);
        return count;
    }

    public void saveSPString(String keySP, String value){
        sp_editor.putString(keySP, value);
        sp_editor.commit();
    }

    public void saveSPInt(String keySP, int value){
        sp_editor.putInt(keySP, value);
        sp_editor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        sp_editor.putBoolean(keySP, value);
        sp_editor.commit();
    }

    public void logout(){
        sp_editor.clear();
        sp_editor.commit();
    }

    public String getSpName(){
        return sp.getString(SP_NAME, "");
    }

    public String getSPEmail(){
        return sp.getString(SP_EMAIL, "");
    }

    public String getSPToken(){
        return sp.getString(SP_TOKEN, "");
    }

    public String getSPRole(){
        return sp.getString(SP_ROLE, "");
    }

    public Boolean getSPIsLogin(){
        return sp.getBoolean(SP_IS_LOGIN, false);
    }

    public String getSPUploadedEvent() { return sp.getString(SP_UPLOADED_EVENT, ""); }

    public String getSpUploadedVolunteer() { return sp.getString(SP_UPLOADED_VOLUNTEER, ""); }
}