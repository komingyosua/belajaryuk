package com.app.belajaryuk.Personal.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.belajaryuk.Adapter.FavoritedPagerAdapter;
import com.app.belajaryuk.R;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritedFragment extends Fragment {
    TextView activity, volunteer, donation;
    private ViewPager viewPager;

    public FavoritedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorited, container, false);
//        activity = view.findViewById(R.id.btn_activity_favorited);
//        volunteer = view.findViewById(R.id.btn_volunteer_favorited);
//        donation = view.findViewById(R.id.btn_donasi_favorited);
//        StatusBarUtil.setColorForSwipeBack(getActivity(), Color.WHITE,0);
//        activity.setOnClickListener(this);
//        volunteer.setOnClickListener(this);
//        donation.setOnClickListener(this);
        viewPager = view.findViewById(R.id.viewpagerFavorited);
        FavoritedPagerAdapter favoritedPagerAdapter = new FavoritedPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(favoritedPagerAdapter);
        SmartTabLayout viewPagerTab = view.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(pageChangeListener);

        return view;
    }


    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener(){

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            if(position == 0) {
//                activity.setBackground(getResources().getDrawable(R.drawable.fill_grey));
//                activity.setTextColor(Color.WHITE);
//            }else if(position == 1){
//                volunteer.setBackground(getResources().getDrawable(R.drawable.fill_grey));
//                volunteer.setTextColor(Color.WHITE);
//            }else if(position == 2){
//                donation.setBackground(getResources().getDrawable(R.drawable.fill_grey));
//                donation.setTextColor(Color.WHITE);
//            }
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.btn_activity_favorited:
//                viewPager.setCurrentItem(0);
//                Log.d("pressed", "activity");
//                break;
//            case R.id.btn_volunteer_favorited:
//                viewPager.setCurrentItem(1);
//                Log.d("pressed", "volunteer");
//                break;
//            case R.id.btn_donasi_favorited:
//                viewPager.setCurrentItem(2);
//                Log.d("pressed", "donation");
//        }
//    }
}
