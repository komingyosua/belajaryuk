package com.app.belajaryuk.Personal.Fragment.History;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.belajaryuk.Adapter.DonationHistoryAdaptor;
import com.app.belajaryuk.Data.HistoryDonation;
import com.app.belajaryuk.Data.UserDonation;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DonationFragment extends Fragment {
    RecyclerView rv_donation;
    private ArrayList<UserDonation> dataUserDonation;
    private DonationHistoryAdaptor donationAdapter;
    Button find;
    private DonationDataService service;
    SharedPrefManager sharedPrefManager;
    protected String token;

    public DonationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

            sharedPrefManager = new SharedPrefManager(getActivity());
            token = sharedPrefManager.getSPToken();
            Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
            service = ServiceGenerator.getClient().create(DonationDataService.class);

//        addData();
        loadDataUserDonation();
            View view;
//        if(data.size()> 0){
            view = inflater.inflate(R.layout.fragment_donation, container, false);
            rv_donation = view.findViewById(R.id.rv_donation_favhistory);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            donationAdapter = new DonationHistoryAdaptor(getContext());
            rv_donation.setLayoutManager(layoutManager);
            rv_donation.setAdapter(donationAdapter);
//        }else{
//            view = inflater.inflate(R.layout.favhistory_donation_not_found, container, false);
//            find = view.findViewById(R.id.btn_findDonation_favhistory);
//            find.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(view.getId() == R.id.btn_findDonation_favhistory){
//                        Intent toDonation = new Intent(getContext(), Donation.class);
//                    }
//                }
//            });
//        }

        return view;
    }

    void loadDataUserDonation(){
        Call<BaseResponse<List<HistoryDonation>>> call = service.listDonation(token);
        call.enqueue(new Callback<BaseResponse<List<HistoryDonation>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<HistoryDonation>>> call, Response<BaseResponse<List<HistoryDonation>>> response) {
                if(response.code() == 200){
                    if(response.body() != null){
                        donationAdapter.addAll(response.body().getData());
                        Log.d("get data : ", response.body().getData().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<HistoryDonation>>> call, Throwable t) {
                Log.d("ERRRORR : ", t.toString());
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        donationAdapter = new DonationHistoryAdaptor(getContext());
        rv_donation.setAdapter(donationAdapter);
    }

}
