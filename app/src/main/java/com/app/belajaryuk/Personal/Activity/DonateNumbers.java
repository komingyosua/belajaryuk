package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.belajaryuk.Data.UserDonation;
import com.app.belajaryuk.Network.Response.DonationResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonateNumbers extends AppCompatActivity implements View.OnClickListener{

    EditText num;
    Button btnPool;
    ImageView img;
    TextView tv;
    private String category;
    private int index;
    protected String token;
    protected String id;
    protected String donationDetailId;
    SharedPrefManager sharedPrefManager;
    DonationDataService donationDataService;
    Date date = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate_numbers);
        num = findViewById(R.id.input_num_of_barang);
        btnPool = findViewById(R.id.buttonAddPool);
        btnPool.setOnClickListener(this);
        img = findViewById(R.id.image_donation);
        tv = findViewById(R.id.tv_donation_numbers);

        category = getIntent().getStringExtra("category");
        index = getIntent().getIntExtra("index", 0);
        id = getIntent().getStringExtra("id");

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT DETAIL : ", sharedPrefManager.getSPToken());
        donationDataService = ServiceGenerator.getClient().create(DonationDataService.class);

        Log.d("CATEGORY : ", category);

        if(category.equals("Uang")){
            img.setImageDrawable(getResources().getDrawable(R.drawable.ic_uang_donation));
            tv.setText("Berapa banyak yang ingin anda donasikan? (Dalam Rupiah)");
        }

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        date = c.getTime();

    }

    void setDonationDetailId(String id){
        this.donationDetailId = id;
        Log.d("Donation Id :", donationDetailId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddPool:
                //TODO LOGIC JOIN
                donate();

                break;
        }
    }

    void donate(){
        Call<DonationResponse<UserDonation>> call = donationDataService.donate(token,id,Integer.parseInt(num.getText().toString()), date.toString());
        call.enqueue(new Callback<DonationResponse<UserDonation>>() {
            @Override
            public void onResponse(Call<DonationResponse<UserDonation>> call, Response<DonationResponse<UserDonation>> response) {
                if(response.code() == 200){
                    if(response.body().getId() != null){
                        Intent join = new Intent(DonateNumbers.this, DonationConfirmation.class);
                        join.putExtra("user-donation-id", response.body().getId());
                        Log.d("USER-DONATION-ID : ", response.body().getId());
                        join.putExtra("category", category);
                        startActivity(join);
                        Toast.makeText(DonateNumbers.this, "Berhasil!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<DonationResponse<UserDonation>> call, Throwable t) {
                Log.d("ERRORNYA BRODIE", t.toString());
            }
        });
    }
}
