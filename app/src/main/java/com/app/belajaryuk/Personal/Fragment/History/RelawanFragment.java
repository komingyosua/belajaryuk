package com.app.belajaryuk.Personal.Fragment.History;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.belajaryuk.Adapter.VolunteerAdapter;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RelawanFragment extends Fragment {
    RecyclerView rv_volunteer;
    private ArrayList<Volunteer> data;
    private VolunteerAdapter volAdapter;
    Button find;

    public RelawanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        addData();
        if(data.size() > 0 ){
            view = inflater.inflate(R.layout.fragment_relawan, container, false);
            rv_volunteer = view.findViewById(R.id.rv_relawan_favhistory);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            volAdapter = new VolunteerAdapter(getContext());
            rv_volunteer.setLayoutManager(layoutManager);
            rv_volunteer.setAdapter(volAdapter);
        }else{
            view = inflater.inflate(R.layout.favhistory_volunteer_not_found, container, false);
            find = view.findViewById(R.id.btn_findVolunteer_favhistory);
            find.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(view.getId() == R.id.btn_findVolunteer_favhistory){
                        Intent toVolunteer = new Intent(getContext(), Volunteer.class);
                        startActivity(toVolunteer);
                    }
                }
            });
        }
        return view;
    }

    void addData(){
        data = new ArrayList<>();
//        data.add(new Volunteer(0,"11 / 100 Peserta","Relawan Pengajar Sasindo27", "12 Agustus 2019 09:00", "yuk... buat kalian yang menyukai dunia anak silahkan bergabung di komunitas kami ,komunitas sasindo27 adalah komunitas yang berbasis pendidikan yang awalnya menjadi dunia literasi dan kini akan dikembangkan untuk pengembangan anak muda indonesia.", "Palmerah, Jakarta Barat", "https://www.indorelawan.org/uploads/gallery/2019-8-13/5d7ba731686ae5e9b0d13767_thumbnail370x250.JPG"));
        //data.add(new Volunteer(1,"90 / 100 Peserta","Penterjemah Bahasa Isyarat Berjiwa Sosial","10 Juli 2019, 09:00", "","Palmerah, Jakarta Barat", "https://www.indorelawan.org/uploads/gallery/2019-8-10/5d778e5902583aa3a49da26e_thumbnail370x250.jpg"));
        //data.add(new Volunteer(2,"28 / 100 Peserta","English Teacher for SD", "8 Juli 2019, 11:00","","Palmerah, Jakarta Barat", "https://www.indorelawan.org/uploads/gallery/2019-8-10/5d777acc02583aa3a49d9e01_thumbnail370x250.jpg"));
        //data.add(new Volunteer(3,"78 / 100 Peserta","Setia Darma House", "8 Juli 2019, 11:00","","Palmerah, Jakarta Barat", "https://quex-indonesia.com/wp-content/uploads/2019/06/Event-PRJ-2019.png"));
    }
}
