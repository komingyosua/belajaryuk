package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.EventAdapter;
import com.app.belajaryuk.Adapter.SliderAdapterEvent;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindEvent extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = FindEvent.class.getSimpleName();

    TextView totalCount;
    ImageButton backEvent;
    Spinner spinnerEvent;
    RecyclerView recyclerEvent;
    SliderView sliderEvent;
    private String token;
    private ArrayList<Event> data;
    private EventAdapter eventAdapter;
    private EventDataService service;
    SharedPrefManager sharedPrefManager;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_find_event);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        StatusBarUtil.setColor(this,Color.WHITE,0);
        totalCount = findViewById(R.id.total_count_event);

        /* Image Back Button */
        backEvent = findViewById(R.id.back_to_main_event);
        backEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        /* Spinner Button */
        spinnerEvent = findViewById(R.id.spinner_view_event);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sort_activities, R.layout.spinner_custom_layout);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEvent.setAdapter(adapter);
        spinnerEvent.setOnItemSelectedListener(this);


        /* Add Data to RecycleView
        addData();*/

        /* Initialize RecycleView */
        recyclerEvent = findViewById(R.id.recycler_view_event);
        eventAdapter = new EventAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FindEvent.this);
        recyclerEvent.setLayoutManager(layoutManager);
        recyclerEvent.setAdapter(eventAdapter);
        loadData();

        //TODO Total Event
        totalCount.setText("TODO");

        /* Initialize Slider */
        sliderEvent = findViewById(R.id.image_slider_event);
        SliderAdapterEvent eventSlider = new SliderAdapterEvent(this);
        sliderEvent.setSliderAdapter(eventSlider);
        sliderEvent.setIndicatorSelectedColor(getResources().getColor(R.color.primaryColor));
        sliderEvent.startAutoCycle();

    }

    private void loadData() {
        Call<BaseResponse<List<Event>>> call = service.apiRead(token);
        call.enqueue(new Callback<BaseResponse<List<Event>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        eventAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {
                Log.e(TAG+".error", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    /*void addData(){
        data = new ArrayList<>();
        data.add(new Event(0, "Belajar Matematika Kelas SMP", "Belajar matematika sambil bermain games-games sehingga suasana belajar menyenangkan tidak seperti di kelas, di sekolah. Hasil yang diharapkan setelah belajar anak – anak bisa menyelesaikan soal matematika dengan mudah.", "Palmerah, Jakarta Barat", "5 Oktober 2019 09:00", "https://www.indorelawan.org/uploads/gallery/2019-8-16/5d7f2d64136a329473937199_thumbnail370x250.jpg"));
        data.add(new Event(1, "Membuat Blog dan Menulis Artikel", "content","Duren Sawit, Jakarta Timur", "15 September 2019 11:00", "https://i0.wp.com/joglosemarnews.com/images/2019/05/290519sd-pk-kottabarat.jpeg?fit=1040%2C780&ssl=1"));
    }*/

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

