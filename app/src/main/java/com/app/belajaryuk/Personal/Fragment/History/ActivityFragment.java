package com.app.belajaryuk.Personal.Fragment.History;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.belajaryuk.Adapter.EventHistoryAdapter;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityFragment extends Fragment {
    private ArrayList<Event> data;
    private EventHistoryAdapter adapter;
    RecyclerView recycler_activity;
    Button find;
    private EventDataService service;
    SharedPrefManager sharedPrefManager;
    protected String token;

    public ActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        sharedPrefManager = new SharedPrefManager(getActivity());
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);
        loadDataHistoryActivity();

//        if(GlobalIndex.getInstance().totalHistoryActivity > 0) {
            view = inflater.inflate(R.layout.fragment_activity, container, false);
            recycler_activity = view.findViewById(R.id.rv_activity_favhistory);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            adapter = new EventHistoryAdapter(getContext());
            recycler_activity.setLayoutManager(layoutManager);
            recycler_activity.setAdapter(adapter);
//        }else{
//            view = inflater.inflate(R.layout.favhistory_activity_not_found, container, false);
//            find = view.findViewById(R.id.btn_findActivity_favhistory);
//            find.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(view.getId() == R.id.btn_findActivity_favhistory){
//                        Intent toActivity = new Intent(getContext(), FindEvent.class);
//                        startActivity(toActivity);
//                    }
//                }
//            });
//        }


        return view;
    }

    void loadDataHistoryActivity(){
        Call<BaseResponse<List<Event>>> call = service.listJoined(token);
        call.enqueue(new Callback<BaseResponse<List<Event>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        adapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new EventHistoryAdapter(getContext());
        recycler_activity.setAdapter(adapter);
    }

}
