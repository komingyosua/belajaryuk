package com.app.belajaryuk.Personal.Fragment;


import android.animation.Animator;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.ArticleHomeAdapter;
import com.app.belajaryuk.Adapter.TodayActivityAdapter;
import com.app.belajaryuk.Adapter.VolunteerFeaturedAdapter;
import com.app.belajaryuk.Data.Article;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.Personal.Activity.Donation;
import com.app.belajaryuk.Personal.Activity.FindEvent;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Personal.Activity.Volunteer;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    LinearLayout search_activity, volunteer_activity, donation_activity;
    TextView see_all_text_activity, greetingsName, greetingsActivity,see_all_text_volunteer;
    ImageView viewGradient, cloud;
    LinearLayout menu;
    RecyclerView today_activity, rv_article_home, rv_volunteer;
    ScrollView scrollview;
    private ArrayList<Event> data;
    private ArrayList<com.app.belajaryuk.Data.Volunteer> dataVolunteer;
    private ArrayList<Article> dataArticle;
    private TodayActivityAdapter eventAdapter;
    private ArticleHomeAdapter article_home_adapter;
    private VolunteerFeaturedAdapter volAdapter;
    final Handler handler = new Handler();
    public static int countAnim = 0;
    private EventDataService service;
    private VolunteerDataService serviceVolunteer;
    private String token;
    SharedPrefManager sharedPrefManager;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ShimmerFrameLayout shimmerVolunteer;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        sharedPrefManager = new SharedPrefManager(getActivity());
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);
        serviceVolunteer = ServiceGenerator.getClient().create(VolunteerDataService.class);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_featured_event);
        shimmerVolunteer = view.findViewById(R.id.shimmer_featured_volunteer);
        search_activity = view.findViewById(R.id.search_activity);
        see_all_text_activity = view.findViewById(R.id.see_all_text_activity);
        see_all_text_volunteer = view.findViewById(R.id.see_all_text_volunteer);
        volunteer_activity = view.findViewById(R.id.volunteer_activity);
        donation_activity = view.findViewById(R.id.donation_activity);
        greetingsName = view.findViewById(R.id.greetings_txt);
        greetingsActivity = view.findViewById(R.id.greetings_text_2);
        viewGradient = view.findViewById(R.id.viewGradient);
        cloud = view.findViewById(R.id.cloudImg);
        menu = view.findViewById(R.id.menuBox);
        search_activity.setOnClickListener(this);
        volunteer_activity.setOnClickListener(this);
        donation_activity.setOnClickListener(this);
        see_all_text_activity.setOnClickListener(this);
        see_all_text_volunteer.setOnClickListener(this);
//        StatusBarUtil.setColorForSwipeBack(getActivity(), getResources().getColor(R.color.primaryColor),0);
        scrollview = view.findViewById(R.id.scrollView);
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_UP);
            }
        });

        Resources res = getResources();
        String text = String.format(res.getString(R.string.greetings_text_personal), "Personal");
        Log.d("GET NAMA : ", sharedPrefManager.getSpName());
        greetingsName.setText(text);
        //LOAD ITEM HERE
        loadDataEvent();
        loadDataVolunteer();


        today_activity = view.findViewById(R.id.today_activity);
        eventAdapter = new TodayActivityAdapter(getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        today_activity.setLayoutManager(layoutManager);
        today_activity.setAdapter(eventAdapter);

        rv_volunteer = view.findViewById(R.id.volunteer_highlight);
        volAdapter = new VolunteerFeaturedAdapter(getActivity());
        RecyclerView.LayoutManager layoutManagerVolunteer = new LinearLayoutManager(getActivity());
        rv_volunteer.setLayoutManager(layoutManagerVolunteer);
        rv_volunteer.setAdapter(volAdapter);

        addDataArticle();
        rv_article_home = view.findViewById(R.id.rv_article_home);
        article_home_adapter = new ArticleHomeAdapter(getActivity(), dataArticle);
        RecyclerView.LayoutManager layoutManagerArticle = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL,false);
        rv_article_home.setLayoutManager(layoutManagerArticle);
        rv_article_home.setAdapter(article_home_adapter);

        if(countAnim<1) {
            animationIntro();
        }else{
            menu.setVisibility(View.VISIBLE);
            greetingsActivity.setVisibility(View.VISIBLE);
            viewGradient.setVisibility(View.VISIBLE);
            greetingsName.setVisibility(View.VISIBLE);
            cloud.setVisibility(View.VISIBLE);
        }

        countAnim++;

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        shimmerVolunteer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        shimmerVolunteer.startShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.search_activity:
            case R.id.see_all_text_activity:
                Intent search_activity = new Intent(getContext(), FindEvent.class);
                startActivity(search_activity);
                break;
            case R.id.volunteer_activity:
            case R.id.see_all_text_volunteer:
                Intent volunteer_activity = new Intent(getContext(), Volunteer.class);
                startActivity(volunteer_activity);
                break;
            case R.id.donation_activity:
                Intent donation_activity = new Intent(getContext(), Donation.class);
                startActivity(donation_activity);
                break;
        }
    }

    void loadDataEvent(){
        Call<BaseResponse<List<Event>>> call = service.featured(token);
        call.enqueue(new Callback<BaseResponse<List<Event>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        eventAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {
                Log.e(TAG+".error", t.toString());
            }
        });
    }


    void loadDataVolunteer(){
        Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call = serviceVolunteer.featured(token);
        call.enqueue(new Callback<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call, Response<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        volAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }else{
                    Log.d("RESPONSE : ", "NO RESPONSE");
                }
                shimmerVolunteer.stopShimmerAnimation();
                shimmerVolunteer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call, Throwable t) {
                Log.d("FAILURE : ", t.toString());
            }
        });
    }

    void addDataArticle(){
        dataArticle = new ArrayList<>();
        dataArticle.add(new Article(0,"Belajar Matematika Bersama","Buat kamu yang pengen belajar matematikan bareng dengan teman-teman yang memiliki minat yang sama. Ayo segera daftarkan diri untuk mencari wawasan lebih luas.","http://files.farhanadji.com/belajaryuk/article_activity/1.jpg","2019-08-21T14:00:32Z","Admin"));
        dataArticle.add(new Article(1,"Yuk buat Blog bareng!","Ayo kita belajar buat blog bareng dengan berbagai metode yang dijelaskan secara rinci oleh mentor yang begitu pengalaman di bidangnya. Oleh karena itu kamu tidak akan rugi","http://files.farhanadji.com/belajaryuk/article_activity/2.jpg","2019-08-21T14:00:32Z","Admin"));
        dataArticle.add(new Article(2,"Belajar SBMPTN GRATIS!","Ayo kita belajar SBMPTN gratis! langsung saja download aplikasi BelajarPintar dan masukkan kode : BELAJARPINTARYUK dan dapatkan 6 bulan konten premium gratis!","http://files.farhanadji.com/belajaryuk/article_activity/3.jpg","2019-08-21T14:00:32Z","Admin"));
    }

    void animationIntro(){
        YoYo.with(Techniques.FadeIn)
                .onStart(new YoYo.AnimatorCallback() {
                    @Override
                    public void call(Animator animator) {
                        greetingsName.setVisibility(View.VISIBLE);
                    }
                })
                .duration(1000)
                .playOn(greetingsName);

        YoYo.with(Techniques.FadeIn)
                .onStart(new YoYo.AnimatorCallback() {
                    @Override
                    public void call(Animator animator) {
                        greetingsActivity.setVisibility(View.VISIBLE);
                    }
                })
                .duration(1000)
                .playOn(greetingsActivity);

        YoYo.with(Techniques.Landing)
                .onStart(new YoYo.AnimatorCallback() {
                    @Override
                    public void call(Animator animator) {
                        viewGradient.setVisibility(View.VISIBLE);
                    }
                })
                .duration(1000)
                .playOn(viewGradient);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.FadeInUp)
                        .onStart(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                menu.setVisibility(View.VISIBLE);
                            }
                        })
                        .duration(1000)
                        .playOn(menu);
            }
        },500);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.FadeInUp)
                        .onStart(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                cloud.setVisibility(View.VISIBLE);
                            }
                        })
                        .duration(1000)
                        .playOn(cloud);
            }
        },1000);
    }
}
