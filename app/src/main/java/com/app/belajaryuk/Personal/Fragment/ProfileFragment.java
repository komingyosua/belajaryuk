package com.app.belajaryuk.Personal.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Personal;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.PersonalDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.Personal.Activity.EditPassword;
import com.app.belajaryuk.Personal.Activity.EditProfile;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {
    CircleImageView profile;
    TextView name, editProfile, editPassword, logout, role;
    SharedPrefManager sharedPrefManager;
    private String id;
    private ArrayList<Personal> currentPerson = new ArrayList<>();
    private String token;
    PersonalDataService servicePersonal;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        StatusBarUtil.setColor(getActivity(), Color.WHITE,0);

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        profile = view.findViewById(R.id.profileImgView);
        name = view.findViewById(R.id.profileName);
        role = view.findViewById(R.id.profileRole);
        sharedPrefManager = new SharedPrefManager(getContext());
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN ASSIGNED : ", token);

        editPassword = view.findViewById(R.id.tv_editPassword);
        editPassword.setOnClickListener(this);
        editProfile = view.findViewById(R.id.tv_editProfile);
        editProfile.setOnClickListener(this);
        logout = view.findViewById(R.id.tv_logout);
        logout.setOnClickListener(this);


        Log.d("TOKEN PROFILE : ", sharedPrefManager.getSPToken());
        servicePersonal = ServiceGenerator.getClient().create(PersonalDataService.class);


        loadProfileData();
       return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_editProfile:
                Intent edit = new Intent(getContext(), EditProfile.class);
                edit.putExtra("data", currentPerson);
                Log.d("ID : ", String.valueOf(currentPerson.get(0).getIndex()));
                startActivity(edit);
                break;
            case R.id.tv_editPassword:
                Intent editPass = new Intent(getContext(), EditPassword.class);
                startActivity(editPass);
                break;

            case R.id.tv_logout:
                sharedPrefManager.logout();
                Intent logout = new Intent(getContext(), WelcomeActivity.class);
                startActivity(logout);
                Toast.makeText(getContext(), "Logout Berhasil!", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                break;
        }
    }

   void loadProfileData(){
       Call<BaseResponse<List<Personal>>> call = servicePersonal.apiPersonalData(token);
       call.enqueue(new Callback<BaseResponse<List<Personal>>>() {
           @Override
           public void onResponse(Call<BaseResponse<List<Personal>>> call, Response<BaseResponse<List<Personal>>> response) {
               if(response.code() == 200){
                   Glide.with(getActivity()).load(Endpoint.API_URL +
                           Endpoint.API_PERSONAL +
                           Endpoint.API_PICTURE +
                           response.body().getData().get(0).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(profile);
                   name.setText(response.body().getData().get(0).getName());
                   role.setText("Personal");
                   Log.d("ERROR : ", response.body().getData().toString());
                   currentPerson.add(response.body().getData().get(0));
               }
           }

           @Override
           public void onFailure(Call<BaseResponse<List<Personal>>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
           }
       });
    }
}
