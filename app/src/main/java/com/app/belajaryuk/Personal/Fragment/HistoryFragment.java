package com.app.belajaryuk.Personal.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.belajaryuk.Adapter.HistoryPagerAdapter;
import com.app.belajaryuk.R;
import com.ogaclejapan.smarttablayout.SmartTabLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    private ViewPager viewPager;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        StatusBarUtil.setColor(getActivity(), Color.WHITE,0);
        View view = inflater.inflate(R.layout.fragment_history, container, false);


        viewPager = view.findViewById(R.id.viewpagerHistory);
        HistoryPagerAdapter historyPagerAdapter = new HistoryPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(historyPagerAdapter);
        SmartTabLayout viewPagerTab = view.findViewById(R.id.viewpagertabHistory);
        viewPagerTab.setViewPager(viewPager);
//        viewPager.addOnPageChangeListener(pageChangeListener);
        return view;
    }

}
