package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.app.belajaryuk.Data.Personal;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.PersonalDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPassword extends AppCompatActivity {
    ImageButton back;
    EditText old, newpass, confirmpass;
    Button submit;
    private String token;
    PersonalDataService servicePersonal;
    SharedPrefManager sharedPrefManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);

        sharedPrefManager = new SharedPrefManager(EditPassword.this);
        token = sharedPrefManager.getSPToken();
        servicePersonal = ServiceGenerator.getClient().create(PersonalDataService.class);

        back = findViewById(R.id.backEditPassProfile);
        old = findViewById(R.id.et_OldPass);
        newpass = findViewById(R.id.et_newPass);
        confirmpass = findViewById(R.id.et_confirmPass);
        submit = findViewById(R.id.buttonUpdatePassProfile);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.buttonUpdatePassProfile){
                    submitChangePassword();
                }
            }
        });

    }

    void submitChangePassword(){
        Call<BaseResponse<List<Personal>>> call = servicePersonal.changePassword(token, old.getText().toString(), newpass.getText().toString(), confirmpass.getText().toString());
        call.enqueue(new Callback<BaseResponse<List<Personal>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Personal>>> call, Response<BaseResponse<List<Personal>>> response) {
                if(response.code() == 200){
                    Toast.makeText(EditPassword.this, "Password berhasil diubah!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Personal>>> call, Throwable t) {
                Log.d("ERROR FAILURE : ", t.toString());
            }
        });
    }
}
