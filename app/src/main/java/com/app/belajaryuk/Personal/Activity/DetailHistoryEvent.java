package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Data.EventDetail;
import com.app.belajaryuk.Data.Personal;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.Service.PersonalDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailHistoryEvent extends AppCompatActivity {
    TextView name, eventname, eventlocation, eventdatetime;
    ImageButton backEvent;
    Button btnCancel;
    protected int index;
    protected String id;
    protected ArrayList<Event> data;
    protected User user;
    private EventDataService service;
    private PersonalDataService personalDataService;
    SharedPrefManager sharedPrefManager;
    protected String token;
    protected String name_id;
    private String eventName;
    private String userName;
    private String date;
    private String time;
    private String location;
    private String qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history_event);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);
        personalDataService = ServiceGenerator.getClient().create(PersonalDataService.class);

        index = getIntent().getIntExtra("index", 0);
        id = getIntent().getStringExtra("id");



        name = findViewById(R.id.name_eventHistory);
        eventname = findViewById(R.id.eventname_eventHistory);
        eventlocation = findViewById(R.id.eventLocation_eventHistory);
        eventdatetime = findViewById(R.id.eventDate_eventHistory);
        btnCancel = findViewById(R.id.buttonCancelActivity);
        loadDataEvent();
//
//        Log.d("EVENT NAME : " , eventName);
//        Log.d("EVENT DATE TIME : ", date + " " + time);
//        Log.d("EVENT LOCATION : ", location);
//        Log.d ("USERNAME : ", userName);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.buttonCancelActivity){
                    deleteHistoryEvent();
                    Toast.makeText(DetailHistoryEvent.this, "Cancel berhasil!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }
        });
        /* Image Back Button */
        backEvent = findViewById(R.id.back_to_home);
        backEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void loadDataEvent(){
        Call<BaseResponse<EventDetail>> call = service.detailEvent(token,id);
        call.enqueue(new Callback<BaseResponse<EventDetail>>() {
            @Override
            public void onResponse(Call<BaseResponse<EventDetail>> call, Response<BaseResponse<EventDetail>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        eventName = response.body().getData().getTitle();
                        Log.d("EVENT NAME : ", response.body().getData().getTitle());
                        date = response.body().getData().getDate();
                        time = response.body().getData().getTime();
                        location = response.body().getData().getPlace();
                        name_id = response.body().getData().getPersonal_id();
                        qr = response.body().getData().getQr();
                        loadDataPersonal(name_id);
                        eventname.setText(eventName);
                        Log.d("NAMA BAWAH : ", eventName);
                        eventdatetime.setText(date + " " + time);
                        eventlocation.setText(location);

                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<EventDetail>> call, Throwable t) {

            }
        });
    }

    void loadDataPersonal(String name_id){
        Call<BaseResponse<Personal>> call = personalDataService.apiReadDetail(token, name_id);
        call.enqueue(new Callback<BaseResponse<Personal>>() {
            @Override
            public void onResponse(Call<BaseResponse<Personal>> call, Response<BaseResponse<Personal>> response) {
                if(response.code() == 200){
                    if(response.body().getData() != null){
                        userName = response.body().getData().getName();
                        name.setText(userName);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<Personal>> call, Throwable t) {

            }
        });

    }

    void deleteHistoryEvent(){
        Call<BaseResponse<EventDetail>> call = service.deleteHistory(token, id);
        call.enqueue(new Callback<BaseResponse<EventDetail>>() {
            @Override
            public void onResponse(Call<BaseResponse<EventDetail>> call, Response<BaseResponse<EventDetail>> response) {
                Toast.makeText(DetailHistoryEvent.this, "Batal Berhasil!", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }

            @Override
            public void onFailure(Call<BaseResponse<EventDetail>> call, Throwable t) {

            }
        });
    }
}
