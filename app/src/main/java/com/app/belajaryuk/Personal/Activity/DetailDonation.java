package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.app.belajaryuk.Community.Activity.FormDonationCommunity;
import com.app.belajaryuk.Data.Donation;
import com.app.belajaryuk.Data.FavoriteDonation;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.ImagePopup;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.Utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailDonation extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    ImageView imgBanner;
    private boolean isHideToolbarView = false;
    private FrameLayout category_behavior;
    private LinearLayout titleAppbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private final String personal = "personal";
    private final String community = "community";
    private TextView appbar_title, category, time, title, desc;
    private ArrayList<Donation> dataList = new ArrayList<>();
    private DonationDataService service;
    private int index;
    private String id;
    private ImageView icon;
    private String categoryDonation;
    protected String token;
    private String type;
    Menu menu;
    View itemChooser;
    Button btnDonate,btnEdit, btnDelete;
    SharedPrefManager sharedPrefManager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(type.equals(personal)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.favorite_button, menu);
            MenuItem menuItem = menu.getItem(0);
            if (menuItem.getItemId() == R.id.favorite) {
                itemChooser = MenuItemCompat.getActionView(menuItem);
                if (itemChooser != null) {

                    itemChooser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.favorite:
                                    Toast.makeText(DetailDonation.this, "Berhasil menambahkan ke favorit", Toast.LENGTH_LONG).show();
                                    addFavorite();
                                    break;
                            }
                        }
                    });
                }
            }
        }

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_detail_donation);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN DONATION : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(DonationDataService.class);

        dataList = getIntent().getParcelableArrayListExtra("dataDonation");
        index = getIntent().getIntExtra("index", 0);
        categoryDonation = dataList.get(index).getCategory();
        id = getIntent().getStringExtra("id");
        type = getIntent().getStringExtra("type");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imgBanner = findViewById(R.id.img_detail_donation);
        appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(this);
        category_behavior = findViewById(R.id.category_behavior);
        titleAppbar = findViewById(R.id.title_appbar);
        appbar_title = findViewById(R.id.title_on_appbar);
        category = findViewById(R.id.category_detail_donation);
        time = findViewById(R.id.time_detail_donation);
        title = findViewById(R.id.title_detail_donation);
        desc = findViewById(R.id.desc_detail_donation);
        icon = findViewById(R.id.icon_detail_donation);


        btnDonate = findViewById(R.id.btn_join_detailDonation);
        btnDonate.setOnClickListener(this);

        /* init button */
        btnDelete = findViewById(R.id.btn_delete_detailDonation);
        btnEdit = findViewById(R.id.btn_edit_detailDonation);
        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        /* VALIDATES FOR TYPE*/
        if(type.equals(personal)){
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnDonate.setVisibility(View.VISIBLE);
        }else if(type.equals(community)){
            btnEdit.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);
            btnDonate.setVisibility(View.GONE);
        }


        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());


        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_READ_VOLUNTEER +
                Endpoint.API_PICTURE +
                dataList.get(index).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imgBanner);

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional


        imagePopup.initiatePopupWithGlide(dataList.get(index).getPicture());

        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopupWithGlide(Endpoint.API_URL +
                        Endpoint.API_READ_DONATION +
                        Endpoint.API_PICTURE +
                        dataList.get(index).getPicture());
                imagePopup.viewPopup();
            }
        });

        appbar_title.setText(dataList.get(index).getTitle());
        if(categoryDonation.equals("Uang")){
            category.setText("Bantuan Dana");
            icon.setBackground(ContextCompat.getDrawable(this, R.drawable.money));

        }else{
            category.setText("Bantuan Barang");
            icon.setBackground(ContextCompat.getDrawable(this, R.drawable.logistic));
        }
        title.setText(dataList.get(index).getTitle());
        time.setText("by " + dataList.get(index).getCreator() + " \u2022 " + Utils.DateToTimeFormat("2019-08-22T14:00:32Z"));
        desc.setText(dataList.get(index).getDesc());
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorPrimary));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;


        if (percentage == 1f && isHideToolbarView) {
            category_behavior.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            category_behavior.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_join_detailDonation:
                //TODO LOGIC JOIN
                if(categoryDonation.equals("Uang")){
                    Intent uang = new Intent(this, DonateNumbers.class);
                    uang.putExtra("category", "Uang");
                    uang.putExtra("index", index);
                    uang.putExtra("id",id);
                    startActivity(uang);
                }else{
                    Intent barang = new Intent(this, DonateNumbers.class);
                    barang.putExtra("category", "Barang");
                    barang.putExtra("id",id);
                    barang.putExtra("index", index);
                    startActivity(barang);
                }
                finish();
                break;
            case R.id.btn_delete_detailDonation:
                //TODO LOGIC DELETE
                delete();
                onBackPressed();
                finish();
                break;
            case R.id.btn_edit_detailDonation:
                //TODO LOGIC JOIN
                Intent join = new Intent(this, FormDonationCommunity.class);
                join.putExtra("donations", dataList);
                join.putExtra("type", "edit");
                join.putExtra("index", index);
                join.putExtra("id", id);
                startActivity(join);
                finish();
                break;
        }
    }

    void delete(){
        Call<BaseResponse<List<Donation>>> call = service.deleteDonation(token,id);
        call.enqueue(new Callback<BaseResponse<List<Donation>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Donation>>> call, Response<BaseResponse<List<Donation>>> response) {
                if(response.code() == 200){
                    Toast.makeText(DetailDonation.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Donation>>> call, Throwable t) {
            }
        });
    }

        void addFavorite(){
            Call<BaseResponse<List<FavoriteDonation>>> call = service.createFavorite(token,id);
            call.enqueue(new Callback<BaseResponse<List<FavoriteDonation>>>() {
                @Override
                public void onResponse(Call<BaseResponse<List<FavoriteDonation>>> call, Response<BaseResponse<List<FavoriteDonation>>> response) {
                    if (response.code() == 200) {
                        Intent fav = new Intent(DetailDonation.this, Personal.class);
                        startActivity(fav);
                        finish();

                    }
                }

                public void onFailure(Call<BaseResponse<List<FavoriteDonation>>> call, Throwable t) {

                }
            });
        }

    void deleteFavorite () {
        Call<BaseResponse<List<FavoriteDonation>>> callFav = service.deleteFavorite(token, id);
        callFav.enqueue(new Callback<BaseResponse<List<FavoriteDonation>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<FavoriteDonation>>> call, Response<BaseResponse<List<FavoriteDonation>>> response) {
                if (response.code() == 200) {
                    Toast.makeText(DetailDonation.this, "Berhasil menghapus donation ini dari favorit!", Toast.LENGTH_SHORT).show();
                    Intent unfav = new Intent(DetailDonation.this, Personal.class);
                    startActivity(unfav);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<FavoriteDonation>>> call, Throwable t) {

            }
        });
    }
}
