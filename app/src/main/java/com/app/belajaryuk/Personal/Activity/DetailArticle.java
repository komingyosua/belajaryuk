package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.app.belajaryuk.Data.Article;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.ImagePopup;
import com.app.belajaryuk.Utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

public class DetailArticle extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {
    ImageView imgBanner;
    private boolean isHideToolbarView = false;
    private FrameLayout date_behavior;
    private LinearLayout titleAppbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private TextView appbar_title, category, time, title, content;
    private List<Article> articles = new ArrayList<>();
    private int articleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_detail_article);

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        articles = getIntent().getParcelableArrayListExtra("dataArticle");
        articleId = getIntent().getIntExtra("articleId", 0);

        toolbar = findViewById(R.id.toolbar_article);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_article);
        collapsingToolbarLayout.setTitle("");
        Toolbar toolbar = findViewById(R.id.toolbar_article);
        setSupportActionBar(toolbar);


        imgBanner = findViewById(R.id.img_detail_article);
        date_behavior = findViewById(R.id.date_behavior);
        titleAppbar = findViewById(R.id.title_appbar_article);
        appBarLayout = findViewById(R.id.appbar_article);
        appBarLayout.addOnOffsetChangedListener(this);
        appbar_title = findViewById(R.id.title_on_appbar_article);
        category = findViewById(R.id.category_detail_article);
        time = findViewById(R.id.time_detail_article);
        title = findViewById(R.id.title_detail_article);
        content = findViewById(R.id.content_detail_article);

        Glide.with(this).load(articles.get(articleId).getUrlPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imgBanner);
        imagePopup.initiatePopupWithGlide(articles.get(articleId).getUrlPicture());

        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopupWithGlide(articles.get(articleId).getUrlPicture());
                imagePopup.viewPopup();
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());

        appbar_title.setText(articles.get(articleId).getTitle()); //fill
        title.setText(articles.get(articleId).getTitle()); //fill
        //2019-08-22T14:00:32Z
        time.setText("by " + articles.get(articleId).getAuthor() + " \u2022 " + Utils.DateToTimeFormat(articles.get(articleId).getPublishedAt()));
        content.setText(articles.get(articleId).getContent());
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorPrimary));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            date_behavior.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            date_behavior.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }
}
