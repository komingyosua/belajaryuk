package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.belajaryuk.Data.UserDonation;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonationConfirmation extends AppCompatActivity implements View.OnClickListener {

    TextView qty, dateShow, status;
    protected String id;
    private String category;
    protected int numQty;
    LinearLayout ll_uang, ll_barang, ll_active;
    Button cancel,proof,disabled;
    protected String token;
    SharedPrefManager sharedPrefManager;
    DonationDataService donationDataService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_confirmation);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT DETAIL : ", sharedPrefManager.getSPToken());
        donationDataService = ServiceGenerator.getClient().create(DonationDataService.class);

        id = getIntent().getStringExtra("user-donation-id");
        if(id != null) {
            Log.d("ID EVENT : ", id);
        }else{
            Log.d("ID EVENT : ", "NULL");
        }
        category = getIntent().getStringExtra("category");


        qty = findViewById(R.id.qty_donation);
        dateShow = findViewById(R.id.date_donation_confirmation);
        status = findViewById(R.id.status_donation_confirmation);

        cancel = findViewById(R.id.btnCancelDonation);
        proof = findViewById(R.id.btn_uploadproof);
        disabled = findViewById(R.id.btn_disabled_donation_confirmation);
        cancel.setOnClickListener(this);
        proof.setOnClickListener(this);
        ll_uang = findViewById(R.id.ll_uang_donation);
        ll_barang = findViewById(R.id.ll_barang_donation);
        ll_active = findViewById(R.id.ll_process);
        getDonation();
    }

    void setQty(int Qty){
        this.numQty = Qty;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnCancelDonation){
            cancelDonation();
        }else if(view.getId() == R.id.btn_uploadproof){
            Toast.makeText(this, "TODO: UPLOAD", Toast.LENGTH_SHORT).show();
        }
    }

    void getDonation(){
        Call<BaseResponse<UserDonation>> call = donationDataService.detail(token,id);
        call.enqueue(new Callback<BaseResponse<UserDonation>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserDonation>> call, Response<BaseResponse<UserDonation>> response) {
                if(response.code() == 200){
                    if(response.body() != null){
                        setQty(response.body().getData().getTotal_given());
                        numQty = response.body().getData().getTotal_given();
                        Log.d("TOTAL GIVEN : ", String.valueOf(response.body().getData().getTotal_given()));
                        status.setText(response.body().getData().getStatus());
                        dateShow.setText(response.body().getData().getSend_date());
                        if(category.equals("Uang")){
                            qty.setText("Rp. " + numQty);
                            ll_uang.setVisibility(View.VISIBLE);
                            ll_barang.setVisibility(View.GONE);

                        }else{
                            qty.setText(numQty + " Barang");
                            ll_barang.setVisibility(View.VISIBLE);
                            ll_uang.setVisibility(View.GONE);
                        }

                        if(response.body().getData().getStatus().equalsIgnoreCase("MENUNGGU PENGIRIMAN")){
                            ll_active.setVisibility(View.VISIBLE);
                            disabled.setVisibility(View.GONE);
                        }else if(response.body().getData().getStatus().equals("SELESAI")){
                            ll_active.setVisibility(View.GONE);
                            disabled.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserDonation>> call, Throwable t) {

            }
        });
    }

    void cancelDonation(){
        Call<BaseResponse<UserDonation>> call = donationDataService.cancel(token, id);
        call.enqueue(new Callback<BaseResponse<UserDonation>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserDonation>> call, Response<BaseResponse<UserDonation>> response) {
                if(response.code() == 200){
                    Toast.makeText(DonationConfirmation.this, "Donasi dibatalkan!", Toast.LENGTH_LONG).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserDonation>> call, Throwable t) {

            }
        });
    }
}
