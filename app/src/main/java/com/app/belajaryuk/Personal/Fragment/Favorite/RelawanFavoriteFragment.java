package com.app.belajaryuk.Personal.Fragment.Favorite;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.belajaryuk.Adapter.VolunteerFavoriteAdapter;
import com.app.belajaryuk.Data.FavoriteVolunteer;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RelawanFavoriteFragment extends Fragment {
    RecyclerView rv_volunteer;
    private ArrayList<Volunteer> data = new ArrayList<>();
    private VolunteerFavoriteAdapter volAdapter;
    Button find;
    private VolunteerDataService service;
    SharedPrefManager sharedPrefManager;
    protected String token;

    public RelawanFavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
//        if(data.size() > 0 ){
            sharedPrefManager = new SharedPrefManager(getActivity());
            token = sharedPrefManager.getSPToken();
            Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
            service = ServiceGenerator.getClient().create(VolunteerDataService.class);

            loadData();

            view = inflater.inflate(R.layout.fragment_relawan, container, false);
            rv_volunteer = view.findViewById(R.id.rv_relawan_favhistory);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            volAdapter = new VolunteerFavoriteAdapter(getContext());
            rv_volunteer.setLayoutManager(layoutManager);
            rv_volunteer.setAdapter(volAdapter);
//        }else{
//            view = inflater.inflate(R.layout.favhistory_volunteer_not_found, container, false);
//            find = view.findViewById(R.id.btn_findVolunteer_favhistory);
//            find.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(view.getId() == R.id.btn_findVolunteer_favhistory){
//                        Intent toVolunteer = new Intent(getContext(), Volunteer.class);
//                        startActivity(toVolunteer);
//                    }
//                }
//            });
//        }
        return view;
    }

    void loadData(){
        Call<BaseResponse<List<FavoriteVolunteer>>> call = service.listFavorited(token);
        call.enqueue(new Callback<BaseResponse<List<FavoriteVolunteer>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<FavoriteVolunteer>>> call, Response<BaseResponse<List<FavoriteVolunteer>>> response) {
                if(response.code() == 200){
                    if(response.body().getData() != null){
                        volAdapter.addAll(response.body().getData());
                        Log.d("DATA GET : ", response.body().getData().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<FavoriteVolunteer>>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        volAdapter = new VolunteerFavoriteAdapter(getContext());
        rv_volunteer.setAdapter(volAdapter);
    }

}
