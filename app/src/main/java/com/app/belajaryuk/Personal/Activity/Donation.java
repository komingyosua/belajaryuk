package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.ArticleDonationAdapter;
import com.app.belajaryuk.Adapter.DonationAdapter;
import com.app.belajaryuk.Adapter.SliderAdapter;
import com.app.belajaryuk.Data.Article;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Donation extends AppCompatActivity implements View.OnClickListener {
    ImageView imgHighlight;
    SliderView sliderView;
    ScrollView scrollview;
    RecyclerView recyclerView, rvArticle;
    TextView showAll;
    private ArrayList<com.app.belajaryuk.Data.Donation> data;
    private ArrayList<Article> dataArticle;
    private DonationAdapter donationAdapter;
    private ArticleDonationAdapter articleDonationAdapter;
    private DonationDataService service;
    SharedPrefManager sharedPrefManager;
    private ShimmerFrameLayout shimmer;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_donation);
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(DonationDataService.class);

        shimmer = findViewById(R.id.shimmer_featured_donation);
        showAll = findViewById(R.id.txt_showAllofDonation);
        showAll.setOnClickListener(this);
        scrollview = findViewById(R.id.scrollViewDonation);
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_UP);
            }
        });

        imgHighlight = findViewById(R.id.img_highlightDonation);

        StatusBarUtil.setColor(this, getResources().getColor(R.color.night),0);

        Glide.with(this).load("https://kelblimbing.malangkota.go.id/wp-content/uploads/sites/79/2016/02/DSC_0370_e.gif").centerCrop().into(imgHighlight);

        //SLIDER
        sliderView = findViewById(R.id.imageSlider);
        final SliderAdapter adapter = new SliderAdapter(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorSelectedColor(getResources().getColor(R.color.primaryColor));
        sliderView.startAutoCycle();

        //RECYCLEVIEW LIST DONATION
        loadDataDonation();
        recyclerView = findViewById(R.id.rv_viewDonation);
        donationAdapter = new DonationAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Donation.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(donationAdapter);

        //RECYCLEVIEW LIST ARTICLE
        addDataArticle();
        rvArticle = findViewById(R.id.rv_ArticleDonation);
        articleDonationAdapter = new ArticleDonationAdapter(Donation.this, dataArticle);
        RecyclerView.LayoutManager layoutManagerArticle = new LinearLayoutManager(Donation.this, RecyclerView.HORIZONTAL,false);
        rvArticle.setLayoutManager(layoutManagerArticle);
        rvArticle.setAdapter(articleDonationAdapter);

    }

    void loadDataDonation(){
        Call<BaseResponse<List<com.app.belajaryuk.Data.Donation>>> call = service.featured(token);
        call.enqueue(new Callback<BaseResponse<List<com.app.belajaryuk.Data.Donation>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<com.app.belajaryuk.Data.Donation>>> call, Response<BaseResponse<List<com.app.belajaryuk.Data.Donation>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        donationAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
                shimmer.stopShimmerAnimation();
                shimmer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<com.app.belajaryuk.Data.Donation>>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmer.stopShimmerAnimation();
        super.onPause();
    }


    void addDataArticle(){
        dataArticle = new ArrayList<>();
        dataArticle.add(new Article(0,"Donasi Online Semakin Laris!","Pemanfaatan platform digital untuk menggalang dana dan berdonasi secara online kian jadi pilihan masyarakat untuk membantu sesama. Dari tahun ke tahun tren tersebut terus mengalami peningkatan.\n" +
                "\n" +
                "Begitulah kecenderungan yang tercatat dalam laporan Indonesia Online Giving Report 2018 di Kitabisa.com, platform untuk menggalang dana dan berdonasi online di Indonesia.\n" +
                "\n" +
                "Sejak awal berdiri pada 2013, Kitabisa melaporkan donasi terkumpul mencapai Rp 892 juta pada tahun 2014. Di tahun berikutnya melonjak hingga Rp 7,2 miliar. Hingga yang meroket dari tahun 2017 ke 2018, di mana donasi terkumpul Rp 257 miliar.","https://www.indorelawan.org/uploads/gallery/2019-8-13/5d7ba731686ae5e9b0d13767_thumbnail370x250.JPG","2019-08-21T14:00:32Z","Admin"));
        dataArticle.add(new Article(1,"Terima Kasih sudah bantu kami!","BelajarYuk! telah menyalurkan donasi pendidikan ke sekolah-sekolah korban gempa bumi. terima kasih kepada kalian yang sudah membantu adik-adik lainnya untuk kembali bersekolah! :)","http://files.farhanadji.com/belajaryuk/article_activity/2.jpg","2019-08-21T14:00:32Z","Admin"));
        dataArticle.add(new Article(2,"Pendidikan untuk Semua!","BelajarYuk! sebagai platform super di dunia pendidikan sudah menyalurkan dana hingga total lebih dari 800 juta rupiah dalam kurun waktu 3 bulan.. terima kasih kepada kalian semua.. sehingga banyak anak-anak Indonesia bisa kembali bersekolah.. :)","http://files.farhanadji.com/belajaryuk/article_activity/3.jpg","2019-08-21T14:00:32Z","Admin"));
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.txt_showAllofDonation){
            Intent showAll = new Intent(this, AllDonation.class);
            startActivity(showAll);
        }
    }
}
