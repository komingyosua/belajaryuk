package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.app.belajaryuk.Community.Activity.AktivitasCommunity;
import com.app.belajaryuk.Community.Activity.FormEventCommunity;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Data.FavoriteEvent;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Response.EventResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.ImagePopup;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.Utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailEvent extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    ImageView imgBanner;
    private String type;
    private final String personal = "personal";
    private final String community = "community";
    private LinearLayout items_layout;
    private boolean isHideToolbarView = false;
    private FrameLayout date_behavior;
    private LinearLayout titleAppbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private TextView appbar_title, category, time, title, content, place;
    private ArrayList<Event> events = new ArrayList<>();
    private EventDataService service;
    private int eventId;
    private String id;
    private String token;
    View itemChooser;
    Button btnJoin, btnEdit, btnDelete;
    SharedPrefManager sharedPrefManager;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(type.equals(personal)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.favorite_button, menu);
            MenuItem menuItem = menu.getItem(0);
            if (menuItem.getItemId() == R.id.favorite) {
                itemChooser = MenuItemCompat.getActionView(menuItem);
                if (itemChooser != null) {
                    itemChooser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.favorite:
                                    //TODO FAVORITE
                                    addFavorite();
                                    Toast.makeText(DetailEvent.this, "Berhasil menambahkan ke favorit", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }
                    });
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_detail_event);

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT DETAIL : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);

        events = getIntent().getParcelableArrayListExtra("dataEvent");
        type = getIntent().getStringExtra("type");
        eventId = getIntent().getIntExtra("index", 0);
        id = getIntent().getStringExtra("id");
        Log.d("ID EVENT : ", id);

        toolbar = findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_event);
        collapsingToolbarLayout.setTitle("");
        Toolbar toolbar = findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);


        imgBanner = findViewById(R.id.image_detail_event);
        date_behavior = findViewById(R.id.date_behavior_event);
        titleAppbar = findViewById(R.id.title_appbar_event);
        appBarLayout = findViewById(R.id.appbar_event);
        appBarLayout.addOnOffsetChangedListener(this);
        appbar_title = findViewById(R.id.title_on_appbar_event);
        time = findViewById(R.id.time_detail_event);
        place = findViewById(R.id.place_detail_event);
        title = findViewById(R.id.title_detail_event);
        content = findViewById(R.id.content_detail_event);

        /* init button */
        btnJoin = findViewById(R.id.btn_join_detailEvent);
        btnDelete = findViewById(R.id.btn_delete_detailEvent);
        btnEdit = findViewById(R.id.btn_edit_detailEvent);
        btnJoin.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        /* VALIDATES FOR TYPE*/
        if (type.equals(personal)) {
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnJoin.setVisibility(View.VISIBLE);
        } else if (type.equals(community)) {
            btnEdit.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);
            btnJoin.setVisibility(View.GONE);
        }


        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_READ_EVENT +
                Endpoint.API_PICTURE +
                events.get(eventId).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imgBanner);
        imagePopup.initiatePopupWithGlide(events.get(eventId).getPicture());


        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopupWithGlide(Endpoint.API_URL +
                        Endpoint.API_READ_EVENT +
                        Endpoint.API_PICTURE +
                        events.get(eventId).getPicture());
                imagePopup.viewPopup();
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());

        appbar_title.setText(events.get(eventId).getTitle()); //fill
        title.setText(events.get(eventId).getTitle()); //fill
        place.setText(events.get(eventId).getPlace());
        //2019-08-22T14:00:32Z
        time.setText(events.get(eventId).getDate());
        content.setText(events.get(eventId).getDesc());
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorPrimary));

        Log.d("EVENT : ", events.get(eventId).toString());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            date_behavior.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            date_behavior.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_join_detailEvent:
                //TODO LOGIC JOIN
                join();
                break;
            case R.id.btn_edit_detailEvent:
                //TODO LOGIC EDIT
                Intent edit = new Intent(this, FormEventCommunity.class);
                edit.putExtra("data", events);
                edit.putExtra("eventId", events.get(eventId));
                edit.putExtra("type", "edit");
                startActivity(edit);
                finish();
                break;
            case R.id.btn_delete_detailEvent:
                //TODO DELETE
                Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
                Intent delete = new Intent(this, AktivitasCommunity.class);
                startActivity(delete);
                finish();
                break;
        }
    }

    void join(){
        Call<EventResponse<Event>> call = service.join(token,id);
        call.enqueue(new Callback<EventResponse<Event>>() {
            @Override
            public void onResponse(Call<EventResponse<Event>> call, Response<EventResponse<Event>> response) {
                Log.d("JOIN STATUS : ", "BERHASIL");
                Toast.makeText(DetailEvent.this, "Berhasil bergabung ke " + events.get(eventId).getTitle() + "!", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<EventResponse<Event>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
            }
        });
    }

    void addFavorite(){
        Call<BaseResponse<FavoriteEvent>> call = service.createFavorite(token, id);
        call.enqueue(new Callback<BaseResponse<FavoriteEvent>>() {
            @Override
            public void onResponse(Call<BaseResponse<FavoriteEvent>> call, Response<BaseResponse<FavoriteEvent>> response) {
                if(response.code() == 200){
                    Toast.makeText(DetailEvent.this, "Berhasil mengikuti aktivitas, Cek Riwayat!", Toast.LENGTH_SHORT).show();
                    Intent join = new Intent(DetailEvent.this, Personal.class);
                    startActivity(join);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<FavoriteEvent>> call, Throwable t) {

            }
        });
    }

    void unFavorite(){
        Call<BaseResponse<FavoriteEvent>> call = service.deleteFavorite(token,id);
        call.enqueue(new Callback<BaseResponse<FavoriteEvent>>() {
            @Override
            public void onResponse(Call<BaseResponse<FavoriteEvent>> call, Response<BaseResponse<FavoriteEvent>> response) {
                if(response.code() == 200){
                    Toast.makeText(DetailEvent.this, "Berhasil menghapus aktivitas ini dari favorit!", Toast.LENGTH_SHORT).show();
                    Intent unfav = new Intent(DetailEvent.this, Personal.class);
                    startActivity(unfav);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<FavoriteEvent>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
            }
        });
    }
}
