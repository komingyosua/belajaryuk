package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.DonationAdapter;
import com.app.belajaryuk.Data.Donation;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDonation extends AppCompatActivity {
    ImageButton back;
    RecyclerView recyclerView;
    DonationAdapter donationAdapter;
    ArrayList<Donation> data;
    private DonationDataService service;
    SharedPrefManager sharedPrefManager;
    private ShimmerFrameLayout shimmer;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_donation);
        back = findViewById(R.id.back_to_main_donation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(DonationDataService.class);

        shimmer = findViewById(R.id.shimmer_donation);
        loadDonationData();
        recyclerView = findViewById(R.id.rv_alldonation);
        donationAdapter = new DonationAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AllDonation.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(donationAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmer.startShimmerAnimation();
    }

    void loadDonationData(){
        Call<BaseResponse<List<Donation>>> call = service.apiRead(token);
        call.enqueue(new Callback<BaseResponse<List<Donation>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Donation>>> call, Response<BaseResponse<List<Donation>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        donationAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
                shimmer.stopShimmerAnimation();
                shimmer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Donation>>> call, Throwable t) {

            }
        });
    }

}
