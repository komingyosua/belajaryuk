package com.app.belajaryuk.Personal.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.app.belajaryuk.Network.Service.LoginDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.Personal.Fragment.FavoritedFragment;
import com.app.belajaryuk.Personal.Fragment.HistoryFragment;
import com.app.belajaryuk.Personal.Fragment.HomeFragment;
import com.app.belajaryuk.Personal.Fragment.ProfileFragment;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jaeger.library.StatusBarUtil;

import butterknife.ButterKnife;

import static com.app.belajaryuk.Personal.Fragment.HomeFragment.countAnim;

public class Personal extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    BottomNavigationView bottomNav;
    SharedPrefManager sharedPrefManager;
    LoginDataService service;
    private static Personal instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;

        setContentView(R.layout.activity_personal);
        bottomNav = findViewById(R.id.btmNav);
        loadFragment(new HomeFragment());
        bottomNav.setOnNavigationItemSelectedListener(this);
        StatusBarUtil.setColor(this, getResources().getColor(R.color.purplePrimary),0);

        ButterKnife.bind(this);
        sharedPrefManager = new SharedPrefManager(this);
        Log.d("TOKEN PERSONAL : " ,sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(LoginDataService.class);
    }

    private boolean loadFragment(Fragment fragment){
        if(fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_layer, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()){
            case R.id.home_menu:
                fragment = new HomeFragment();
                break;
            case R.id.favorite_menu:
                fragment = new FavoritedFragment();
                break;
            case R.id.my_activity_menu:
                fragment = new HistoryFragment();
                break;
            case R.id.account_menu:
                fragment = new ProfileFragment();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countAnim = 0;
    }
}
