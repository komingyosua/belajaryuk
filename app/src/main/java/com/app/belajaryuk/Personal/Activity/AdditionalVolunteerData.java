package com.app.belajaryuk.Personal.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.R;

public class AdditionalVolunteerData extends AppCompatActivity implements View.OnClickListener {

    Button btnRegisVol;
    EditText reason;
    EditText uploadResume;
    private String id;
    private VolunteerDataService service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_volunteer_data);

        btnRegisVol = findViewById(R.id.btnRegisVol);
        btnRegisVol.setOnClickListener(this);

        reason = findViewById(R.id.reasonVolunteer);
        uploadResume = findViewById(R.id.uploadFilePDF);
        uploadResume.setOnClickListener(this);

        id = getIntent().getStringExtra("id");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRegisVol:

                Toast.makeText(this, "Berhasil mendaftar relawan, Cek Riwayat!", Toast.LENGTH_SHORT).show();
                Intent join = new Intent(this, Personal.class);
                startActivity(join);
                finish();
                break;
            case R.id.uploadFilePDF:
                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Pilih file PDF"), 1);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            String selected = data.getData().toString();
            uploadResume.setText(selected);
        }
    }

    void submit(){
//        Call<BaseResponse<List<Volunteer>>> call = service.join(id, reason.getText().toString(),)
    }
}
