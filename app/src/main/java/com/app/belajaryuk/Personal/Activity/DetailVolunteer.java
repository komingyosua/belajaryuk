package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.app.belajaryuk.Community.Activity.FormVolunteerCommunity;
import com.app.belajaryuk.Data.FavoriteVolunteer;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.ImagePopup;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.Utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailVolunteer extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    ImageView imgBanner;
    private boolean isHideToolbarView = false;
    private FrameLayout date_behavior;
    private String type;
    private String token;
    private String id;
    private final String personal = "personal";
    private final String community = "community";
    private LinearLayout titleAppbar;
    private AppBarLayout appBarLayout;
    private VolunteerDataService service;
    private Toolbar toolbar;
    private TextView appbar_title, category, time, title, content, place;
    private ArrayList<Volunteer> volunteers = new ArrayList<>();
    private int index;
    Button btnJoin, btnEdit, btnDelete;
    View itemChooser;
    SharedPrefManager sharedPrefManager;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(type.equals(personal)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.favorite_button, menu);
            MenuItem menuItem = menu.getItem(0);
            if (menuItem.getItemId() == R.id.favorite) {
                itemChooser = MenuItemCompat.getActionView(menuItem);
                if (itemChooser != null) {
                    itemChooser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.favorite:
                                    Toast.makeText(DetailVolunteer.this, "Berhasil menambahkan ke favorit", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }
                    });
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_detail_volunteer);

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT DETAIL : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(VolunteerDataService.class);


        volunteers = getIntent().getParcelableArrayListExtra("dataVolunteer");
        index = getIntent().getIntExtra("index", 0);
        type = getIntent().getStringExtra("type");
        id = getIntent().getStringExtra("id");
        Log.d("ID EVENT : ", id);

        Log.d("INDEX : ", String.valueOf(index));

        toolbar = findViewById(R.id.toolbar_vol);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_vol);
        collapsingToolbarLayout.setTitle("");
        Toolbar toolbar = findViewById(R.id.toolbar_vol);
        setSupportActionBar(toolbar);


        imgBanner = findViewById(R.id.image_detail_vol);
        date_behavior = findViewById(R.id.date_behavior_vol);
        titleAppbar = findViewById(R.id.title_appbar_vol);
        appBarLayout = findViewById(R.id.appbar_vol);
        appBarLayout.addOnOffsetChangedListener(this);
        appbar_title = findViewById(R.id.title_on_appbar_vol);
        time = findViewById(R.id.time_detail_vol);
        place = findViewById(R.id.place_detail_vol);
        title = findViewById(R.id.title_detail_vol);
        content = findViewById(R.id.content_detail_vol);

        /* init button */
        btnJoin = findViewById(R.id.btn_join_detailVolunteer);
        btnDelete = findViewById(R.id.btn_delete_detailVolunteer);
        btnEdit = findViewById(R.id.btn_edit_detailVolunteer);
        btnJoin.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        /* VALIDATES FOR TYPE*/
        if(type.equals(personal)){
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnJoin.setVisibility(View.VISIBLE);
        }else if(type.equals(community)){
            btnEdit.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);
            btnJoin.setVisibility(View.GONE);
        }

        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_READ_VOLUNTEER +
                Endpoint.API_PICTURE +
                volunteers.get(index).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imgBanner);
        imagePopup.initiatePopupWithGlide(volunteers.get(index).getPicture());


        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopupWithGlide(Endpoint.API_URL +
                        Endpoint.API_READ_VOLUNTEER +
                        Endpoint.API_PICTURE +
                        volunteers.get(index).getPicture());
                imagePopup.viewPopup();
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());

        appbar_title.setText(volunteers.get(index).getTitle()); //fill
        title.setText(volunteers.get(index).getTitle()); //fill
        place.setText(volunteers.get(index).getPlace());
        //2019-08-22T14:00:32Z
        time.setText(volunteers.get(index).getDate());
        content.setText(volunteers.get(index).getDesc());
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorPrimary));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            date_behavior.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            date_behavior.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_edit_detailVolunteer:
                //TODO LOGIC EDIT
                Intent edit = new Intent(this, FormVolunteerCommunity.class);
                edit.putParcelableArrayListExtra("dataVolunteer",volunteers);
                edit.putExtra("volunteerId", volunteers.get(index).getId());
                edit.putExtra("index", index);
                edit.putExtra("type", "edit");
                startActivity(edit);
                finish();
                break;
            case R.id.btn_delete_detailVolunteer:
                //TODO LOGIC DELETE
                delete();
                onBackPressed();
                finish();
                break;
            case R.id.btn_join_detailVolunteer:
                //TODO LOGIC JOIN
                Intent join = new Intent(this, AdditionalVolunteerData.class);
                join.putExtra("id", volunteers.get(index).getId());
                startActivity(join);
                finish();
                break;
        }
    }

    void delete(){
        Call<BaseResponse<List<Volunteer>>> call = service.deleteVolunteer(token,id);
        call.enqueue(new Callback<BaseResponse<List<Volunteer>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Volunteer>>> call, Response<BaseResponse<List<Volunteer>>> response) {
                if(response.code() == 200){
                    Toast.makeText(DetailVolunteer.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Volunteer>>> call, Throwable t) {

            }
        });
    }

    void addFavorite(){
        Call<BaseResponse<List<FavoriteVolunteer>>> call = service.createFavorite(token, id);
        call.enqueue(new Callback<BaseResponse<List<FavoriteVolunteer>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<FavoriteVolunteer>>> call, Response<BaseResponse<List<FavoriteVolunteer>>> response) {
                if(response.code() == 200){
                    Intent fav = new Intent(DetailVolunteer.this, Personal.class);
                    startActivity(fav);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<FavoriteVolunteer>>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
            }
        });
    }
}
