package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.SliderAdapterVolunteer;
import com.app.belajaryuk.Adapter.VolunteerAdapter;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Volunteer extends AppCompatActivity {

    RecyclerView rv_volunteer;
    private ArrayList<com.app.belajaryuk.Data.Volunteer> data;
    private VolunteerAdapter volAdapter;
    SliderView sliderVolunteer;
    private VolunteerDataService service;
    SharedPrefManager sharedPrefManager;
    private ShimmerFrameLayout shimmer;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteer);
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(VolunteerDataService.class);

        shimmer = findViewById(R.id.shimmer_vounteer_activity);

        StatusBarUtil.setColor(this, Color.WHITE,0);

        ImageButton back = findViewById(R.id.back_to_main_volunteer);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadDataVolunteer();
        rv_volunteer = findViewById(R.id.rv_volunteer);
        volAdapter = new VolunteerAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Volunteer.this);
        rv_volunteer.setLayoutManager(layoutManager);
        rv_volunteer.setAdapter(volAdapter);

        sliderVolunteer = findViewById(R.id.imgSliderVolunteer);
        final SliderAdapterVolunteer slideVol = new SliderAdapterVolunteer(this);
        sliderVolunteer.setSliderAdapter(slideVol);
        sliderVolunteer.setIndicatorSelectedColor(getResources().getColor(R.color.primaryColor));
        sliderVolunteer.startAutoCycle();
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmer.startShimmerAnimation();
        super.onPause();
    }

    void loadDataVolunteer(){
        Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call = service.apiRead(token);
        call.enqueue(new Callback<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call, Response<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> response) {
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        Log.d("DATA : ", response.body().getData().toString());
                        volAdapter.addAll(response.body().getData());
                    } else {
                        Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
                        Log.d("MESSAGE : ", response.body().getMessage());
                    }
                }
                shimmer.stopShimmerAnimation();
                shimmer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<com.app.belajaryuk.Data.Volunteer>>> call, Throwable t) {

            }
        });

    }
}
