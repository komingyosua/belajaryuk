package com.app.belajaryuk.Personal.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Personal;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.PersonalDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    private static int RESULT_LOAD_IMAGE = 1;
    LinearLayout ll_personal;
    EditText phone, bio, dob, address, city, province, postcode;
    TextView hint;
    ImageButton back;
    ImageView imageEditProfile;
    RadioGroup genderGroup;
    RadioButton radioMales, radioFemales;
    RadioButton selectedGenderRadio;
    Button update;
    private ArrayList<Personal> currentPerson = new ArrayList<>();
    SharedPrefManager sharedPrefManager;
    private String id;
    private Integer eventId;
    private String token;
    PersonalDataService servicePersonal;
    ProgressDialog progressDialog;
    String imagePath;
    String gender_values;
    Uri selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EditProfile.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }


        sharedPrefManager = new SharedPrefManager(this);

        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN PERSONALPROF : ", token);
        servicePersonal = ServiceGenerator.getClient().create(PersonalDataService.class);

        currentPerson = getIntent().getParcelableArrayListExtra("data");

        ll_personal = findViewById(R.id.containerFormEditProfile);
        phone = findViewById(R.id.et_phoneNumberProfile);
        bio = findViewById(R.id.et_bioProfile);
        dob = findViewById(R.id.et_dobProfile);
        address = findViewById(R.id.et_addressProfile);
        city = findViewById(R.id.et_cityProfile);
        province = findViewById(R.id.et_provinceProfile);
        postcode = findViewById(R.id.et_postcodeProfile);
        hint = findViewById(R.id.hintTextEditProfile);
        back = findViewById(R.id.backEditProfile);
        imageEditProfile = findViewById(R.id.imageEditProfile);
        genderGroup = findViewById(R.id.genderGroupEditProfile);
        radioMales = findViewById(R.id.radioMale);
        radioFemales = findViewById(R.id.radioFemale);
        update = findViewById(R.id.buttonUpdateProfile);
        update.setOnClickListener(this);
        imageEditProfile.setOnClickListener(this);
        dob.setOnClickListener(this);
        back.setOnClickListener(this);


        phone.setText(currentPerson.get(0).getPhone());
        bio.setText(currentPerson.get(0).getBio());
        dob.setText(currentPerson.get(0).getDob());
        address.setText(currentPerson.get(0).getAddress());
        city.setText(currentPerson.get(0).getCity());
        province.setText(currentPerson.get(0).getProvince());
        postcode.setText(currentPerson.get(0).getPost_code());

        if (currentPerson.get(0).getGender().equals("male")){
            radioMales.setChecked(true);
            radioFemales.setChecked(false);
        } else {
            radioMales.setChecked(false);
            radioFemales.setChecked(true);
        }

        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_PERSONAL +
                Endpoint.API_PICTURE +
                currentPerson.get(0).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imageEditProfile);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonUpdateProfile:
                progressDialog.setMessage("Mohon Menunggu..");
                progressDialog.show();
                File files = new File(imagePath);

                RequestBody phone_update = RequestBody.create(phone.getText().toString(), MediaType.parse("text/plain"));
                RequestBody bio_update = RequestBody.create(bio.getText().toString(), MediaType.parse("text/plain"));
                RequestBody dob_update = RequestBody.create(dob.getText().toString(), MediaType.parse("text/plain"));
                RequestBody address_update = RequestBody.create(address.getText().toString(), MediaType.parse("text/plain"));
                RequestBody city_update = RequestBody.create(city.getText().toString(), MediaType.parse("text/plain"));
                RequestBody province_update = RequestBody.create(province.getText().toString(), MediaType.parse("text/plain"));
                RequestBody postcode_update = RequestBody.create(postcode.getText().toString(), MediaType.parse("text/plain"));

                int selectedGender = genderGroup.getCheckedRadioButtonId();
                if(selectedGender == R.id.radioMale){
                    gender_values = "male";
                }else if(selectedGender == R.id.radioFemale){
                    gender_values = "female";
                }

                RequestBody gender_update = RequestBody.create(gender_values, MediaType.parse("text/plain"));
                RequestBody requestFileImg = RequestBody.create(files, MediaType.parse("image/*"));
                MultipartBody.Part body2 = MultipartBody.Part.createFormData("picture", files.getName(), requestFileImg);

                Call<BaseResponse<Personal>> call = servicePersonal.updatePersonalData(token, phone_update,bio_update,gender_update,dob_update,postcode_update,province_update,city_update,address_update,body2);
                call.enqueue(new Callback<BaseResponse<Personal>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Personal>> call, Response<BaseResponse<Personal>> response) {
                        if(response.code() == 200){
                            onBackPressed();
                            Toast.makeText(EditProfile.this, "Berhasil update profile!", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            Log.d("Error brodie:", response.errorBody().toString());
                        }
                        progressDialog.dismiss();
                    }


                    @Override
                    public void onFailure(Call<BaseResponse<Personal>> call, Throwable t) {
                        Toast.makeText(EditProfile.this, "Gagal", Toast.LENGTH_SHORT).show();
                        Log.d("ERROR : ", t.toString());
                    }
                });
                break;

            case R.id.imageEditProfile:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);
                break;

            case R.id.et_dobProfile:
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfile.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        view.setMaxDate(Calendar.getInstance().getTime().getTime());
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        dob.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.backEditProfile:
                onBackPressed();
                break;
        }
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                // Get the Image from data
                selectedImage = data.getData();
                String selectedFilePath = selectedImage.getPath();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                imagePath = imgDecodableString;
                imageEditProfile.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

                Log.d("GAMBARNYA : ", selectedFilePath);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }
}
