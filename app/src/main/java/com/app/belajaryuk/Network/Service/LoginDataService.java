package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.UserResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginDataService {

    @FormUrlEncoded
    @POST(Endpoint.API_LOGIN)
    Call<UserResponse> login(@Field("email") String email,
                             @Field("password") String password);

    @GET("secretKey")
    Call<ResponseBody> getSecret(@Header("Authorization") String authToken);

    @POST("authRefresh")
    Call<UserResponse> refreshToken(@Header("Authorization") String authToken);


}
