package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.Personal;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;

import java.util.List;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface PersonalDataService {

    @GET(Endpoint.API_PERSONAL)
    Call<BaseResponse<User>> apiRead(
        @Header("Authorization") String auth
    );

    @GET(Endpoint.API_PERSONAL + "/{id}")
    Call<BaseResponse<Personal>> apiReadDetail(
        @Header("Authorization") String auth,
        @Path("id") String id
    );

    @GET(Endpoint.API_PERSONAL)
    Call<BaseResponse<List<Personal>>> apiPersonalData(
        @Header("Authorization") String auth
    );


    @Multipart
    @POST(Endpoint.API_PERSONAL_PROFILE)
    Call<BaseResponse<Personal>> updatePersonalData(
        @Header("Authorization") String authKey,
        @Part("phone") RequestBody phone,
        @Part("bio") RequestBody bio,
        @Part("gender") RequestBody gender,
        @Part("dob") RequestBody dob,
        @Part("post_code") RequestBody post_code,
        @Part("province") RequestBody province,
        @Part("city") RequestBody city,
        @Part("address") RequestBody address,
        @Part MultipartBody.Part image
    );


    @FormUrlEncoded
    @POST(Endpoint.API_CHANGE_PASSWORD)
    Call<BaseResponse<List<Personal>>> changePassword(
        @Header("Authorization") String auth,
        @Field("old_password") String old_password,
        @Field("new_password") String new_password,
        @Field("confirm_password") String confirm_password
    );

}
