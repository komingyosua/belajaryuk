package com.app.belajaryuk.Network.Interceptor;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.app.belajaryuk.App;
import com.app.belajaryuk.IntroApp;
import com.app.belajaryuk.LoginActivity;
import com.app.belajaryuk.Network.Response.UserResponse;
import com.app.belajaryuk.Network.Service.LoginDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenAuthenticator extends AppCompatActivity implements Interceptor {

    SharedPrefManager sharedPrefManager;
    LoginDataService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public TokenAuthenticator() {

        sharedPrefManager = new SharedPrefManager(IntroApp.getContext());
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response mainResponse = chain.proceed(chain.request());
        Request mainRequest = chain.request();
        LoginDataService lds = ServiceGenerator.getClient().create(LoginDataService.class);

        if ( mainResponse.code() == 401 || mainResponse.code() == 403 ) {
            String token = sharedPrefManager.getSPToken();
            retrofit2.Response<UserResponse> refreshToken = service.refreshToken(token).execute();
            if (refreshToken.isSuccessful()) {
                sharedPrefManager.saveSPString(SharedPrefManager.SP_TOKEN, "Bearer " +
                        refreshToken.body().getToken());
                Request.Builder builder = mainRequest.newBuilder().header("Accept", "application/json").header("Authorization",
                        sharedPrefManager.getSPToken())
                        .method(mainRequest.method(), mainRequest.body());
                mainResponse = chain.proceed(builder.build());
            }

            //Jika tidak ingin refresh token dan langsung logout
//            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
//            Intent i = new Intent(MyApp.getContext(), LoginActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            MyApp.getContext().startActivity(i);

        } else if ( mainResponse.code() == 500 ){
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_IS_LOGIN, false);
            Intent i = new Intent(App.getContext(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            App.getContext().startActivity(i);
        }

        return mainResponse;
    }
}