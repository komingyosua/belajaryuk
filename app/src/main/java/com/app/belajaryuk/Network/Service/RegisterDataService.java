package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Response.UserResponse;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RegisterDataService {

    @Multipart
    @POST(Endpoint.API_REGISTER)
    Call<BaseResponse<List<User>>> register(
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("role") RequestBody role
    );

    @GET("secretKey")
    Call<ResponseBody> getSecret(@Header("Authorization") String authToken);

    @POST("authRefresh")
    Call<UserResponse> refreshToken(@Header("Authorization") String authToken);


}
