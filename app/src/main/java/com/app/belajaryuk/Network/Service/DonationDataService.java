package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.Donation;
import com.app.belajaryuk.Data.FavoriteDonation;
import com.app.belajaryuk.Data.HistoryDonation;
import com.app.belajaryuk.Data.UserDonation;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Response.DonationResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface DonationDataService {
    @GET(Endpoint.API_READ_DONATION)
    Call<BaseResponse<List<Donation>>> apiRead(
        @Header("Authorization") String authToken
    );

    @Multipart
    @POST(Endpoint.API_CREATE_DONATION)
    Call<BaseResponse<List<Donation>>> apiCreate(
            @Header("Authorization") String authToken,
            @Part("title") RequestBody title,
            @Part("desc") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("place") RequestBody place,
            @Part("pool") RequestBody pool,
            @Part("creator") RequestBody creator,
            @Part("category") RequestBody category,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST(Endpoint.API_UPDATE_DONATION + "{id}")
    Call<BaseResponse<List<Donation>>> apiUpdate(
            @Header("Authorization") String authToken,
            @Path("id") String id,
            @Part("title") RequestBody title,
            @Part("desc") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("place") RequestBody place,
            @Part("pool") RequestBody pool,
            @Part("creator") RequestBody creator,
            @Part("category") RequestBody category,
            @Part MultipartBody.Part image
    );

    @DELETE(Endpoint.API_DELETE_DONATION+ "{id}")
    Call<BaseResponse<List<Donation>>> deleteDonation(
            @Header("Authorization") String authToken,
            @Path("id") String id
    );

    @GET(Endpoint.API_DONATION_FEATURED)
    Call<BaseResponse<List<Donation>>> featured(
        @Header("Authorization") String authToken
    );

    @FormUrlEncoded
    @POST(Endpoint.API_DONATION_JOIN + "/{id}")
    Call<DonationResponse<UserDonation>> donate(
        @Header("Authorization") String authToken,
        @Path("id") String id,
        @Field("total_given") int total_given,
        @Field("send_date") String send_date
    );

    @GET(Endpoint.API_DONATION_JOIN)
    Call<BaseResponse<List<HistoryDonation>>> listDonation(
        @Header("Authorization") String authToken
    );

    @GET(Endpoint.API_DETAIL_DONATION + "{id}")
    Call<BaseResponse<UserDonation>> detail(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @DELETE(Endpoint.API_DELETE_HISTORY_DONATION + "{id}")
    Call<BaseResponse<UserDonation>> cancel(
        @Header("Authorization") String authToken,
        @Path("id") String id

    );

    @GET(Endpoint.API_FAVORITED_DONATION)
    Call<BaseResponse<List<FavoriteDonation>>> listFavorited(
        @Header("Authorization") String authToken
    );

    @POST(Endpoint.API_CREATE_FAVORITE_DONATION + "{id}")
    Call<BaseResponse<List<FavoriteDonation>>> createFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @DELETE(Endpoint.API_DELETE_FAVORITE_DONATION + "{id}")
    Call<BaseResponse<List<FavoriteDonation>>> deleteFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

}
