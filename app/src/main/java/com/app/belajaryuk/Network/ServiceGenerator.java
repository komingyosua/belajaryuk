package com.app.belajaryuk.Network;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.app.belajaryuk.IntroApp;
import com.app.belajaryuk.Utils.SharedPrefManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator extends AppCompatActivity {

    private static Retrofit retrofit;
    private static SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefManager = new SharedPrefManager(IntroApp.getContext());
    }

    public static Retrofit getClient() {
        String token = "";
        if(sharedPrefManager !=null){
            if(sharedPrefManager.getSPToken()!=null){
                token = sharedPrefManager.getSPToken();
            }
        }
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = null;
        Log.e("TOKEN : ", token);
        if(!token.isEmpty()){
            String token_jwt = token;
            client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(new Interceptor() {
                        @NotNull
                        @Override
                        public Response intercept(@NotNull Chain chain) throws IOException {
                            Request newRequest  = chain.request().newBuilder()
                                    .addHeader("Authorization", "Bearer " + token_jwt)
                                    .build();
                            return chain.proceed(newRequest);
                        }
                    })
                    .build();

        }else{
            client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();
        }
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Endpoint.API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;

    }
}