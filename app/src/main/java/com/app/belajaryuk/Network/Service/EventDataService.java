package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Data.EventDetail;
import com.app.belajaryuk.Data.FavoriteEvent;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Response.EventResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface EventDataService {
    @GET(Endpoint.API_READ_EVENT)
    Call<BaseResponse<List<Event>>> apiRead(
        @Header("Authorization") String authToken
    );

    @Multipart
    @POST(Endpoint.API_CREATE_EVENT)
    Call<BaseResponse<List<Event>>> apiCreate(
        @Header("Authorization") String authToken,
        @Part("title") RequestBody title,
        @Part("desc") RequestBody description,
        @Part("date") RequestBody date,
        @Part("time") RequestBody time,
        @Part("place") RequestBody place,
        @Part MultipartBody.Part image
    );

    @Multipart
    @POST(Endpoint.API_UPDATE_EVENT + "{id}")
    Call<BaseResponse<List<Event>>> apiUpdate(
            @Header("Authorization") String authToken,
            @Path("id") String id,
            @Part("title") RequestBody title,
            @Part("desc") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("place") RequestBody place,
            @Part MultipartBody.Part image
    );

    @DELETE(Endpoint.API_DELETE_EVENT + "{id}")
    Call<BaseResponse<List<Event>>> deleteEvent(
            @Header("Authorization") String authToken,
            @Path("id") String id
    );

    @GET(Endpoint.API_EVENT_FEATURED)
    Call<BaseResponse<List<Event>>> featured(
        @Header("Authorization") String authToken
    );

    @POST(Endpoint.API_EVENT_JOIN + "/{id}")
    Call<EventResponse<Event>> join(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @GET(Endpoint.API_EVENT_JOIN)
    Call<BaseResponse<List<Event>>> listJoined(
        @Header("Authorization") String authToken
    );

    @GET(Endpoint.API_DETAIL_EVENT + "{id}")
    Call<BaseResponse<EventDetail>> detailEvent(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @DELETE(Endpoint.API_DELETE_HISTORY_ACTIVITY + "{id}")
    Call<BaseResponse<EventDetail>> deleteHistory(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    //FAVORITED LIST

    @GET(Endpoint.API_FAVORITED_EVENT)
    Call<BaseResponse<List<FavoriteEvent>>> listFavEvent(
        @Header("Authorization") String authToken
    );

    @POST(Endpoint.API_CREATE_FAVORITE_EVENT + "{id}")
    Call<BaseResponse<FavoriteEvent>> createFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @DELETE(Endpoint.API_DELETE_FAVORITE_EVENT + "{id}")
    Call<BaseResponse<FavoriteEvent>> deleteFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );


}
