package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.Community;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface CommunityDataService {

    @GET(Endpoint.API_COMMUNITY)
    Call<BaseResponse<List<Community>>> apiRead(
            @Header("Authorization") String auth
    );

    @GET(Endpoint.API_COMMUNITY + "/{id}")
    Call<BaseResponse<Community>> apiReadDetail(
            @Header("Authorization") String auth,
            @Path("id") String id
    );

    @GET(Endpoint.API_GET_COMMUNITY_NAME_ONLY + "/{id}")
    Call<BaseResponse<Community>> apiReadName(
            @Header("Authorization") String auth,
            @Path("id") String id
    );

    @Multipart
    @POST(Endpoint.API_COMMUNITY_PROFILE)
    Call<BaseResponse<Community>> updateCommunityData(
            @Header("Authorization") String authKey,
            @Part("phone") RequestBody phone,
            @Part("post_code") RequestBody post_code,
            @Part("province") RequestBody province,
            @Part("city") RequestBody city,
            @Part("address") RequestBody address,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST(Endpoint.API_CHANGE_PASSWORD)
    Call<BaseResponse<List<Community>>> changePassword(
            @Header("Authorization") String auth,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password,
            @Field("confirm_password") String confirm_password
    );

}
