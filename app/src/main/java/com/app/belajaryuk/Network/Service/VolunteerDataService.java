package com.app.belajaryuk.Network.Service;

import com.app.belajaryuk.Data.FavoriteVolunteer;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface VolunteerDataService {

    @GET(Endpoint.API_READ_VOLUNTEER)
    Call<BaseResponse<List<Volunteer>>> apiRead(
        @Header("Authorization") String authToken
    );

    @Multipart
    @POST(Endpoint.API_CREATE_VOLUNTEER)
    Call<BaseResponse<List<Volunteer>>> apiCreate(
            @Header("Authorization") String authToken,
            @Part("title") RequestBody title,
            @Part("desc") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("place") RequestBody place,
            @Part("quota") RequestBody quota,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST(Endpoint.API_UPDATE_VOLUNTEER + "{id}")
    Call<BaseResponse<List<Volunteer>>> apiUpdate(
            @Header("Authorization") String authToken,
            @Path("id") String id,
            @Part("title") RequestBody title,
            @Part("desc") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("place") RequestBody place,
            @Part("quota") RequestBody quota,
            @Part MultipartBody.Part image
    );

    @GET(Endpoint.API_VOLUNTEER_FEATURED)
    Call<BaseResponse<List<Volunteer>>> featured(
        @Header("Authorization") String authToken
    );

    @DELETE(Endpoint.API_DELETE_VOLUNTEER+ "{id}")
    Call<BaseResponse<List<Volunteer>>> deleteVolunteer(
            @Header("Authorization") String authToken,
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST(Endpoint.API_VOLUNTEER_JOIN+"/{id}")
    Call<BaseResponse<List<Volunteer>>> join(
        @Path("id") String id,
        @Field("reason") String reason,
        @Field("other_document") File document
    );

    @GET(Endpoint.API_FAVORITED_VOLUNTEER)
    Call<BaseResponse<List<FavoriteVolunteer>>> listFavorited(
        @Header("Authorization") String authToken
    );


    @POST(Endpoint.API_CREATE_FAVORITE_VOLUNTEER + "{id}")
    Call<BaseResponse<List<FavoriteVolunteer>>> createFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @DELETE(Endpoint.API_DELETE_FAVORITE_EVENT + "{id}")
    Call<BaseResponse<List<FavoriteVolunteer>>> deleteFavorite(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @GET(Endpoint.API_VOLUNTEER_JOIN)
    Call<BaseResponse<List<User>>> listofUserVolunteer(
        @Header("Authorization") String authToken
    );

    @POST(Endpoint.API_VOLUNTEER_ACCEPT + "{id}")
    Call<BaseResponse<User>> acceptUser(
        @Header("Authorization") String authToken,
        @Path("id") String id
    );

    @POST(Endpoint.API_VOLUNTEER_DECLINE + "{id}")
    Call<BaseResponse<User>> declineVolunteer(
            @Header("Authorization") String authToken,
            @Path("id") String id
    );
}
