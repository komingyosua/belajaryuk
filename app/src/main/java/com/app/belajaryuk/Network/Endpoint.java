package com.app.belajaryuk.Network;

public class Endpoint {

    /* Base URL API */

    public static final String API_URL = "https://ancient-plains-22481.herokuapp.com";


    /*Personal*/
    public static final String API_READ_EVENT = "/v1/event";
    public static final String API_READ_DONATION = "/v1/donation";
    public static final String API_READ_VOLUNTEER = "/v1/volunteer";

    public static final String API_DELETE_EVENT = "/v1/event/";
    public static final String API_DELETE_VOLUNTEER = "/v1/volunteer/";
    public static final String API_DELETE_DONATION = "/v1/donation/";

    public static final String API_CREATE_EVENT = "/v1/event";
    public static final String API_CREATE_DONATION = "/v1/donation";
    public static final String API_CREATE_VOLUNTEER = "/v1/volunteer";

    public static final String API_UPDATE_EVENT = "/v1/event/";
    public static final String API_UPDATE_DONATION = "/v1/donation/";
    public static final String API_UPDATE_VOLUNTEER = "/v1/volunteer/";

    public static final String API_LOGIN = "/v1/auth/login";
    public static final String API_REGISTER = "/v1/auth/register";

    public static final String API_PICTURE = "/picture/";

    public static final String API_EVENT_FEATURED = "/v1/event-featured";
    public static final String API_VOLUNTEER_FEATURED = "/v1/volunteer-featured";
    public static final String API_DONATION_FEATURED = "/v1/donation-featured";

    public static final String API_VOLUNTEER_JOIN = "/v1/user-volunteer";
    public static final String API_EVENT_JOIN = "/v1/user-event";
    public static final String API_VOLUNTEER_ACCEPT = "/v1/user-volunteer/accept/";
    public static final String API_VOLUNTEER_DECLINE = "/v1/user-volunteer/decline/";



    public static final String API_PERSONAL = "/v1/personal";
    public static final String API_DETAIL_EVENT = "/v1/user-event/detail/";
    public static final String API_GET_PERSONAL_NAME_ONLY = "/v1/personal/names";
    public static final String API_PERSONAL_PROFILE = "/v1/personal/profile";
    public static final String API_DELETE_HISTORY_ACTIVITY = "/v1/user-event/delete/";

    public static final String API_DONATION_JOIN = "/v1/user-donation";
    public static final String API_DETAIL_DONATION = "/v1/user-donation/detail/";
    public static final String API_DELETE_HISTORY_DONATION = "/v1/user-donation/delete/";

    public static final String API_FAVORITED_EVENT = "/v1/favorited-event/";
    public static final String API_CREATE_FAVORITE_EVENT = "/v1/favorited-event/create/";
    public static final String API_DELETE_FAVORITE_EVENT = "/v1/favorited-event/delete/";

    public static final String API_FAVORITED_VOLUNTEER = "/v1/favorited-voluneer/";
    public static final String API_CREATE_FAVORITE_VOLUNTEER = "/v1/favorited-event/create/";
    public static final String API_DELETE_FAVORITE_VOLUNTEER = "/v1/favorited-event/delete/";


    public static final String API_FAVORITED_DONATION = "/v1/favorited-donation";
    public static final String API_CREATE_FAVORITE_DONATION = "/v1/favorited-donation/create/";
    public static final String API_DELETE_FAVORITE_DONATION = "/v1/favorited-donation/delete/";


    public static final String API_CHANGE_PASSWORD = "/v1/change-password/";

    /*Community*/

    public static final String API_COMMUNITY = "/v1/community";
    public static final String API_GET_COMMUNITY_NAME_ONLY = "/v1/community/names";
    public static final String API_COMMUNITY_PROFILE = "/v1/community/profile";



}