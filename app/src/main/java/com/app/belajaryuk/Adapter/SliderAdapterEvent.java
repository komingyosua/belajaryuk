package com.app.belajaryuk.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

public class SliderAdapterEvent extends SliderViewAdapter<SliderAdapterEvent.SliderAdapterVH> {
    private Context context;

    public SliderAdapterEvent(Context context) {
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item,null);
        return new SliderAdapterVH(view);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        switch (position){
            case 0 :
                Glide.with(viewHolder.itemView)
                        .load("http://files.farhanadji.com/belajaryuk/activity/1.jpg")
                        .centerCrop()
                        .into(viewHolder.imageViewBackground);
                break;
            case 1:
                Glide.with(viewHolder.itemView)
                        .load("http://files.farhanadji.com/belajaryuk/activity/2.jpg")
                        .centerCrop()
                        .into(viewHolder.imageViewBackground);
                break;
            case 2:
                Glide.with(viewHolder.itemView)
                        .load("http://files.farhanadji.com/belajaryuk/activity/3.jpg")
                        .centerCrop()
                        .into(viewHolder.imageViewBackground);
                break;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    public class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
