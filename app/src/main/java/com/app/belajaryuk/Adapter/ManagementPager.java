package com.app.belajaryuk.Adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.belajaryuk.Community.Fragment.ManagementActivity;
import com.app.belajaryuk.Community.Fragment.ManagementDonation;
import com.app.belajaryuk.Community.Fragment.ManagementVolunteer;

public class ManagementPager extends FragmentStatePagerAdapter {
  public ManagementPager(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    switch (position){
      case 0 : return new ManagementActivity();
      case 1 : return new ManagementVolunteer();
      case 2 : return new ManagementDonation();
    }
    return null;
  }

  @Override
  public int getCount() {
    return 3;
  }

  @Nullable
  @Override
  public CharSequence getPageTitle(int position) {
    switch (position) {
      case 0: return "Aktivitas";
      case 1: return "Volunteer";
      case 2: return "Donasi";
    }
    return null;
  }
}
