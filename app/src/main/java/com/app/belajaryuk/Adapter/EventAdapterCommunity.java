package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Community.Activity.DetailEventCommunity;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Global.GlobalIndex;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.R;

import java.util.ArrayList;
import java.util.List;

public class EventAdapterCommunity extends RecyclerView.Adapter<EventAdapterCommunity.ViewHolder> {
  private Context context;
  private ArrayList<Event> data;

  public EventAdapterCommunity(Context context){
    this.context = context;
    data = null;
    data = new ArrayList<>();
    GlobalIndex.getInstance().indexEvent = 0;

  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.custom_layout_find_activity, parent,false);
    return new EventAdapterCommunity.ViewHolder(view);
  }

  public void add(Event item) {
    data.add(item);
    notifyItemInserted(data.size() - 1);
    Log.d("SIZE : ", String.valueOf(data.size()));


  }

  public void addAll(List<Event> items) {
    GlobalIndex.getInstance().indexEvent = 0;
    for (Event item : items) {
      add(item);
    }
  }

  public int sizeIndex(){
    return data.size();
  }

  public Event getData(int position) {
    return data.get(position);
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
    holder.title.setText(data.get(position).getTitle());
    holder.date.setText(data.get(position).getDate());
    holder.place.setText(data.get(position).getPlace());
    Glide.with(context).load(Endpoint.API_URL+
            Endpoint.API_READ_EVENT+
            Endpoint.API_PICTURE+
            data.get(position).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(holder.imgEvent);

    holder.activity_layout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(view.getId() == R.id.find_activity_layout){
          View viewImg = holder.imgEvent;

          Intent intent = new Intent(context, DetailEventCommunity.class);
          intent.putExtra("index", data.get(position).getIndex());
          intent.putExtra("id", data.get(position).getId());
          intent.putExtra("type", "personal");
          intent.putExtra("dataEvent", data);
          Pair<View, String> pair = Pair.create(viewImg, "ImgEvent");
          ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
          if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            context.startActivity(intent, optionsCompat.toBundle());
          }else{
            context.startActivity(intent);
          }

          GlobalIndex.getInstance().indexEvent = 0;
        }
      }
    });
  }

  @Override
  public int getItemCount() {
    return data.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView title, place, date;
    ImageView imgEvent;
    LinearLayout activity_layout;
    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.title_text);
      place = itemView.findViewById(R.id.place_text);
      date = itemView.findViewById(R.id.date_text);
      imgEvent = itemView.findViewById(R.id.image_thumbnail);
      activity_layout = itemView.findViewById(R.id.find_activity_layout);

    }
  }
}
