package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Community.Activity.DetailEventCommunity;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.R;
import java.util.ArrayList;

public class EventCommunityAdapter extends RecyclerView.Adapter<EventCommunityAdapter.EventViewHolder> {
    private Context context;

    private ArrayList<Event> dataList;

    public EventCommunityAdapter(ArrayList<Event> dt, Context context){
        this.dataList = dt;
        this.context = context;
    }
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_layout_list_event_community, parent,false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EventViewHolder holder, final int position) {
        holder.title.setText(dataList.get(position).getTitle());
        holder.date.setText(dataList.get(position).getDate());
        holder.place.setText(dataList.get(position).getPlace());

        Glide.with(context).load(Endpoint.API_URL+
                Endpoint.API_READ_EVENT+
                Endpoint.API_PICTURE+
                dataList.get(position).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(holder.imgEvent);

        holder.activity_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.listEventLayout){
                    View viewImg = holder.imgEvent;
                    Pair<View, String> pair = Pair.create(viewImg, "imgEvent");
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
                    Intent intent = new Intent(context, DetailEventCommunity.class);
                    intent.putExtra("index", dataList.get(position).getIndex());
                    intent.putExtra("id", dataList.get(position).getId());
                    intent.putExtra("dataEvent", dataList);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                        context.startActivity(intent, optionsCompat.toBundle());
                    }else{
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        TextView title, place, date;
        ImageView imgEvent;
        LinearLayout activity_layout;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleListEvent);
            place = itemView.findViewById(R.id.placeListEvent);
            date = itemView.findViewById(R.id.dateListEvent);
            imgEvent = itemView.findViewById(R.id.imageThumbnailListEvent);
            activity_layout = itemView.findViewById(R.id.listEventLayout);
        }
    }
}