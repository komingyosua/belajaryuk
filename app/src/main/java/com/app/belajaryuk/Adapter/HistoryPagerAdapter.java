package com.app.belajaryuk.Adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.belajaryuk.Personal.Fragment.History.ActivityFragment;
import com.app.belajaryuk.Personal.Fragment.History.DonationFragment;
import com.app.belajaryuk.Personal.Fragment.History.RelawanFragment;

public class HistoryPagerAdapter extends FragmentStatePagerAdapter {

    public HistoryPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new ActivityFragment();
            case 1: return new RelawanFragment();
            case 2: return new DonationFragment();
        }
        return  null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return "Aktivitas";
            case 1: return "Volunteer";
            case 2: return "Donasi";
        }
        return null;
    }
}
