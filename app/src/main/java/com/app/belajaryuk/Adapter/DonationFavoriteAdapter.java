package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.FavoriteDonation;
import com.app.belajaryuk.Global.GlobalIndex;
import com.app.belajaryuk.Personal.Activity.DetailDonation;
import com.app.belajaryuk.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DonationFavoriteAdapter extends RecyclerView.Adapter<DonationFavoriteAdapter.ViewHolder> {
    Locale localeID = new Locale("in", "ID");
    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
    private Context context;
    private ArrayList<FavoriteDonation> dataList;

    public DonationFavoriteAdapter(Context context){
        this.dataList = new ArrayList<>();
        this.context = context;
    }

    public void add(FavoriteDonation item) {
        dataList.add(item);
        notifyItemInserted(dataList.size() - 1);
    }

    public void addAll(List<FavoriteDonation> items) {
        GlobalIndex.getInstance().indexDonation = 0;
        for (FavoriteDonation item : items) {
            add(item);
        }
    }

    @NonNull
    @Override
    public DonationFavoriteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.donation_item_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DonationFavoriteAdapter.ViewHolder holder, int position) {
        holder.title.setText(dataList.get(position).getTitle());
        holder.creator.setText(dataList.get(position).getCreator());

        if(dataList.get(position).getCategory() == "Uang"){
            holder.icon.setBackground(ContextCompat.getDrawable(context,R.drawable.money));
            holder.category.setText("Bantuan Dana");
            holder.price.setText("Rp." + dataList.get(position).getPool());
        }else{
            holder.icon.setBackground(ContextCompat.getDrawable(context,R.drawable.logistic));
            holder.category.setText("Bantuan Barang");
            holder.price.setText(dataList.get(position).getPool() + " (pcs)");
        }

        Glide.with(context).load(dataList.get(position).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(holder.imgDonation);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.cv_donation_item:
                        Intent intent = new Intent(context, DetailDonation.class);
                        intent.putExtra("index", dataList.get(position).getIndex());
                        intent.putExtra("dataDonation", dataList);
                        intent.putExtra("id", dataList.get(position).getDonation_id());
                        View viewImg = holder.imgDonation;
                        View viewTitle = holder.title;
                        View viewCreator = holder.creator;
                        Pair<View, String> pair1 = Pair.create(viewImg, "ImgDonation");
//                        Pair<View, String> pair2 = Pair.create(viewTitle, "donationTransitionTitle");
//                        Pair<View, String> pair3 = Pair.create(viewCreator, "donationTransitionCreator");

                        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair1);

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                            context.startActivity(intent, optionsCompat.toBundle());
                        }else{
                            context.startActivity(intent);
                        }

                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, creator, price, category;
        ImageView imgDonation, icon;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_donation_item);
            creator = itemView.findViewById(R.id.creator_donation_item);
            icon = itemView.findViewById(R.id.icon_donation_item);
            category = itemView.findViewById(R.id.txt_donation_item);
            price = itemView.findViewById(R.id.pool_donation_item);
            imgDonation = itemView.findViewById(R.id.img_donation_item);
            cardView = itemView.findViewById(R.id.cv_donation_item);
        }
    }
}
