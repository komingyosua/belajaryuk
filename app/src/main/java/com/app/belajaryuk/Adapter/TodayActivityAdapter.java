package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Global.GlobalIndex;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Personal.Activity.DetailEvent;
import com.app.belajaryuk.R;
import java.util.ArrayList;
import java.util.List;

public class TodayActivityAdapter extends RecyclerView.Adapter<TodayActivityAdapter.EventViewHolder> {
    private Context context;
    private ArrayList<Event> dataList;

    public TodayActivityAdapter(Context context){
        this.dataList = null;
        this.dataList = new ArrayList<>();
        this.context = context;
        GlobalIndex.getInstance().indexEvent = 0;
    }

    public void add(Event item) {
        dataList.add(item);
        notifyItemInserted(dataList.size() - 1);
        Log.d("SIZE : ", String.valueOf(dataList.size()));
    }

    public void addAll(List<Event> items) {
        GlobalIndex.getInstance().indexEvent = 0;
        for (Event item : items) {
            add(item);
        }
    }
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_layout_today_activity, parent,false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        Log.d("INDEX POSITION :", String.valueOf(position));
        holder.title.setText(dataList.get(position).getTitle());
        holder.date.setText(dataList.get(position).getDate());
        holder.place.setText(dataList.get(position).getPlace());

        Glide.with(context).load(Endpoint.API_URL+
                Endpoint.API_READ_EVENT+
                Endpoint.API_PICTURE+
                dataList.get(position).getPicture()).fitCenter().into(holder.imgEvent);
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.lv_today_activity){
                    View viewImg = holder.imgEvent;
                    Intent intent = new Intent(context, DetailEvent.class);
                    intent.putExtra("index", dataList.get(position).getIndex());
                    intent.putExtra("id",dataList.get(position).getId());
                    Log.d("INDEX INTENT : " , String.valueOf(dataList.get(position).getIndex()));
                    intent.putExtra("type", "personal");
                    intent.putExtra("dataEvent", dataList);
                    Pair<View, String> pair = Pair.create(viewImg, "ImgEvent");
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                        context.startActivity(intent, optionsCompat.toBundle());
                    }else{
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        TextView title, place, date;
        ImageView imgEvent;
        LinearLayout ll;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            ll = itemView.findViewById(R.id.lv_today_activity);
            title = itemView.findViewById(R.id.title_text_home);
            date = itemView.findViewById(R.id.date_text_home);
            place = itemView.findViewById(R.id.place_text_home);
            imgEvent = itemView.findViewById(R.id.image_thumbnail_home);
        }
    }
}