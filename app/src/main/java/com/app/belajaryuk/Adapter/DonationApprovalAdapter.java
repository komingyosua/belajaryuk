package com.app.belajaryuk.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.Community.Activity.DetailDonation;
import com.app.belajaryuk.Data.DonationApproval;
import com.app.belajaryuk.R;

import java.util.ArrayList;

public class DonationApprovalAdapter extends RecyclerView.Adapter<DonationApprovalAdapter.ViewHolder> {
  private Context context;
  private ArrayList<DonationApproval> dataList;
  LinearLayout ll;

  public DonationApprovalAdapter(ArrayList<DonationApproval> dataList, Context context){
    this.dataList = dataList;
    this.context = context;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.donation_approval_item, parent,false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
    if(dataList.get(position).getStatus().equals("DITOLAK")){
      holder.status.setTextColor(ContextCompat.getColor(context,R.color.redFailed));
    }else if(dataList.get(position).getStatus().equals("MENUNGGU PENGIRIMAN")){
      holder.status.setTextColor(ContextCompat.getColor(context,R.color.blueWaiting));
    }else if(dataList.get(position).getStatus().equals("DITERIMA")){
      holder.status.setTextColor(ContextCompat.getColor(context,R.color.greenSuccess));
    }
    holder.name.setText(dataList.get(position).getName());
    holder.donationName.setText(dataList.get(position).getDonationName());
    holder.type.setText(dataList.get(position).getType());
    holder.status.setText(dataList.get(position).getStatus());
    Glide.with(context).load(dataList.get(position).getUrlPicture()).fitCenter().into(holder.img);
    holder.ll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(view.getId() == R.id.ll_donation_list){
          Intent ll = new Intent(context, DetailDonation.class);
          ll.putExtra("donationId", dataList.get(position).getId());
          ll.putExtra("data", dataList);
          context.startActivity(ll);
        }
      }
    });

  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView img;
    TextView name, donationName, type, status;
    LinearLayout ll;
    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      img = itemView.findViewById(R.id.img_donation_list);
      name = itemView.findViewById(R.id.name_donation_list);
      donationName = itemView.findViewById(R.id.donate_donation_list);
      type = itemView.findViewById(R.id.jenis_donation_list);
      status = itemView.findViewById(R.id.status_donation_list);
      ll = itemView.findViewById(R.id.ll_donation_list);
    }
  }
}
