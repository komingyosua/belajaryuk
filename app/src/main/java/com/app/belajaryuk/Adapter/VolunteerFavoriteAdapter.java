package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.Data.FavoriteVolunteer;
import com.app.belajaryuk.Global.GlobalIndex;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Personal.Activity.DetailVolunteer;
import com.app.belajaryuk.R;

import java.util.ArrayList;
import java.util.List;

public class VolunteerFavoriteAdapter extends RecyclerView.Adapter<VolunteerFavoriteAdapter.Viewholder> {
    private Context context;
    private ArrayList<FavoriteVolunteer> dataList;

    public VolunteerFavoriteAdapter(Context context){
        this.dataList = new ArrayList<>();
        this.context = context;
        GlobalIndex.getInstance().indexVolunteer = 0;
    }

    public void add(FavoriteVolunteer item) {
        dataList.add(item);
        notifyItemInserted(dataList.size() - 1);
        Log.d("SIZE : ", String.valueOf(dataList.size()));
    }

    public void addAll(List<FavoriteVolunteer> items) {
        GlobalIndex.getInstance().indexVolunteer = 0;
        for (FavoriteVolunteer item : items) {
            add(item);
        }
    }
    @NonNull
    @Override
    public VolunteerFavoriteAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_layout_find_volunteer, parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VolunteerFavoriteAdapter.Viewholder holder, int position) {
        holder.count.setText(dataList.get(position).getQuota());
        holder.title.setText(dataList.get(position).getTitle());
        holder.place.setText(dataList.get(position).getPlace());

        Glide.with(context).load(Endpoint.API_URL+Endpoint.API_READ_VOLUNTEER+Endpoint.API_PICTURE+dataList.get(position).getPicture()).fitCenter().into(holder.imgVol);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.vol_layout){
                    View viewImg = holder.imgVol;
                    Intent intent = new Intent(context, DetailVolunteer.class);
                    intent.putExtra("index", dataList.get(position).getIndex());
                    intent.putExtra("id", dataList.get(position).getVolunteer_id());
                    intent.putExtra("type", "personal");
                    intent.putExtra("dataVolunteer", dataList);
                    Pair<View, String> pair = Pair.create(viewImg, "ImgVolunteer");
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                        context.startActivity(intent, optionsCompat.toBundle());
                    }else{
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView count, title, place;
        ImageView imgVol;
        LinearLayout layout;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            count = itemView.findViewById(R.id.count_peserta);
            title = itemView.findViewById(R.id.title_vol);
            place = itemView.findViewById(R.id.place_vol);
            imgVol = itemView.findViewById(R.id.imageVol);
            layout = itemView.findViewById(R.id.vol_layout);
        }
    }
}
