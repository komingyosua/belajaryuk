package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.Data.Article;
import com.app.belajaryuk.Personal.Activity.DetailArticle;
import com.app.belajaryuk.R;

import java.util.ArrayList;

public class ArticleHomeAdapter extends RecyclerView.Adapter<ArticleHomeAdapter.ArticleViewHolder> {
    private Context context;
    private ArrayList<Article> dataList;

    public ArticleHomeAdapter(Context context, ArrayList<Article> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_article_home_item,parent,false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ArticleViewHolder holder, final int position) {
        holder.title.setText(dataList.get(position).getTitle());
        holder.content.setText(dataList.get(position).getContent());
        Glide.with(context).load(dataList.get(position).getUrlPicture()).centerCrop().into(holder.img);

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.detailBtn_article_list_home){
                    View viewImg = holder.img;
                    Intent intent = new Intent(context, DetailArticle.class);
                    intent.putExtra("articleId", dataList.get(position).getId());
                    intent.putExtra("dataArticle", dataList);
                    Pair<View, String> pair = Pair.create(viewImg, "ImgArticle");
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                        context.startActivity(intent, optionsCompat.toBundle());
                    }else{
                        context.startActivity(intent);
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView title, content, detail;
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_article_list_home);
            title = itemView.findViewById(R.id.title_article_list_home);
            content = itemView.findViewById(R.id.content_article_list_home);
            detail = itemView.findViewById(R.id.detailBtn_article_list_home);
        }
    }
}
