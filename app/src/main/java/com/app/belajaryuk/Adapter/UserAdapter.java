package com.app.belajaryuk.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.belajaryuk.Community.Activity.DetailUser;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.R;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
  private Context context;
  private ArrayList<User> dataList;

  public UserAdapter(ArrayList<User> dataList, Context context){
    this.dataList = dataList;
    this.context = context;
  }

  @NonNull
  @Override
  public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.user_item_list, parent,false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, final int position) {
    holder.name.setText(dataList.get(position).getName());
    holder.event.setText(dataList.get(position).getEvent());
    Glide.with(context).load(dataList.get(position).getUrlProfilePicture()).fitCenter().into(holder.img);
    holder.ll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(view.getId() == R.id.ll_user_list){
          Intent ll = new Intent(context, DetailUser.class);
          ll.putExtra("userId", dataList.get(position).getId());
          ll.putExtra("data", dataList);
          context.startActivity(ll);
        }
      }
    });

  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView img;
    TextView name, event;
    LinearLayout ll;
    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      img = itemView.findViewById(R.id.img_user_list);
      name = itemView.findViewById(R.id.name_user_list);
      event = itemView.findViewById(R.id.event_user_list);
      ll = itemView.findViewById(R.id.ll_user_list);
    }
  }
}
