package com.app.belajaryuk.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Global.GlobalIndex;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Personal.Activity.DetailVolunteer;
import com.app.belajaryuk.R;

import java.util.ArrayList;
import java.util.List;

public class VolunteerAdapterCommunity extends RecyclerView.Adapter<VolunteerAdapterCommunity.ViewHolder> {
  private Context context;
  private ArrayList<Volunteer> dataList;

  public VolunteerAdapterCommunity(Context context){
    this.context = context;
    dataList = null;
    dataList = new ArrayList<>();
    GlobalIndex.getInstance().indexVolunteer = 0;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.custom_layout_find_volunteer, parent,false);
    return new VolunteerAdapterCommunity.ViewHolder(view);
  }

  public void add(Volunteer item) {
    dataList.add(item);
    notifyItemInserted(dataList.size() - 1);
    Log.d("SIZE : ", String.valueOf(dataList.size()));


  }

  public void addAll(List<Volunteer> items) {
    GlobalIndex.getInstance().indexVolunteer = 0;
    for (Volunteer item : items) {
      add(item);
    }
  }

  public int sizeIndex(){
    return dataList.size();
  }

  public Volunteer getData(int position) {
    return dataList.get(position);
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
    holder.count.setText(dataList.get(position).getQuota() + " Peserta");
    holder.title.setText(dataList.get(position).getTitle());
    holder.place.setText(dataList.get(position).getPlace());

    Glide.with(context).load(Endpoint.API_URL+
            Endpoint.API_READ_VOLUNTEER+
            Endpoint.API_PICTURE+
            dataList.get(position).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(holder.imgVol);

    holder.layout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(view.getId() == R.id.vol_layout){
          View viewImg = holder.imgVol;
          Pair<View, String> pair = Pair.create(viewImg, "imgVol");
          ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context,pair);
          Intent intent = new Intent(context, DetailVolunteer.class);
          intent.putExtra("volunteerId", dataList.get(position).getId());
          intent.putExtra("index", dataList.get(position).getIndex());
          intent.putExtra("id", dataList.get(position).getId());
          intent.putExtra("type", "community");
          intent.putExtra("dataVolunteer", dataList);
          if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            context.startActivity(intent, optionsCompat.toBundle());
            ((Activity) context).finish();
          }else{
            context.startActivity(intent);
            ((Activity) context).finish();
          }

          GlobalIndex.getInstance().indexVolunteer = 0;
        }
      }
    });
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView count, title, place;
    ImageView imgVol;
    LinearLayout layout;
    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      count = itemView.findViewById(R.id.count_peserta);
      title = itemView.findViewById(R.id.title_vol);
      place = itemView.findViewById(R.id.place_vol);
      imgVol = itemView.findViewById(R.id.imageVol);
      layout = itemView.findViewById(R.id.vol_layout);
    }
  }
}
