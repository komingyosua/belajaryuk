package com.app.belajaryuk;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.RegisterDataService;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.List;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Button registerBtn;
    RegisterDataService registerDataService;
    EditText name;
    EditText email;
    EditText password;
    Spinner spinner;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    Context mContext;
    private static final String[] role = {"Community", "Personal"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = this;
        sharedPrefManager = new SharedPrefManager(RegisterActivity.this);

        name = findViewById(R.id.ET_name_register);
        email = findViewById(R.id.ET_username_register);
        password = findViewById(R.id.password_register);
        registerBtn = findViewById(R.id.btn_register);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        spinner = findViewById(R.id.SP_role_register);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterActivity.this,
                android.R.layout.simple_spinner_item,role);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        registerBtn.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
//              Intent intentLogin = new Intent(this, Personal.class);
                progressDialog = ProgressDialog.show(this,null,"Mohon menunggu..",true,false);
                registers();
//                startActivity(intentLogin);
                break;
        }
    }

    private void registers(){
        RequestBody name_data = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
        RequestBody email_data = RequestBody.create(email.getText().toString(), MediaType.parse("text/plain"));
        RequestBody password_data = RequestBody.create(password.getText().toString(), MediaType.parse("text/plain"));
        RequestBody spinner_data = RequestBody.create(spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString().toLowerCase(), MediaType.parse("text/plain"));

        Call<BaseResponse<List<User>>> call = registerDataService.register(name_data, email_data,password_data, spinner_data);

        call.enqueue(new Callback<BaseResponse<List<User>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<User>>> call, Response<BaseResponse<List<User>>> response) {
                if(response.code() == 200) {
                    Intent intentLogin = new Intent(RegisterActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentLogin);
                    finish();
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(RegisterActivity.this,"Email/Password salah!!", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<User>>> call, Throwable t) {
                Log.d("ERROR LOGIN : ", t.toString());
            }
        });

    }
}
