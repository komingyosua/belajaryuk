package com.app.belajaryuk;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.belajaryuk.Community.Activity.Community;
import com.app.belajaryuk.Network.Response.UserResponse;
import com.app.belajaryuk.Network.Service.LoginDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.Personal.Activity.Personal;
import com.app.belajaryuk.Utils.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvForgotPass;
    TextView lbl_register;
    Button loginBtn;
    LoginDataService loginDataService;
    TextView email;
    TextView passw;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_login);
        tvForgotPass = findViewById(R.id.lbl_forgotpass_form_login);
        loginBtn = findViewById(R.id.btn_masuk);
        lbl_register = findViewById(R.id.lbl_register);
        email = findViewById(R.id.ET_username_login);
        passw = findViewById(R.id.password_login);

        tvForgotPass.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        lbl_register.setOnClickListener(this);

        mContext = this;
        sharedPrefManager = new SharedPrefManager(LoginActivity.this);

        Log.d("ROLE : ", sharedPrefManager.getSPRole());

        if (sharedPrefManager.getSPIsLogin() && sharedPrefManager.getSPRole().equals("personal")){
            startActivity(new Intent(LoginActivity.this, Personal.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }else if(sharedPrefManager.getSPIsLogin() && sharedPrefManager.getSPRole().equals("community")){
            startActivity(new Intent(LoginActivity.this, Community.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        loginDataService = ServiceGenerator.getClient().create(LoginDataService.class);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lbl_forgotpass_form_login:
                Intent intentForgot = new Intent(this,ForgotPassActivity.class);
                startActivity(intentForgot);
                break;
            case R.id.lbl_register:
                Intent intentRegister = new Intent(this, RegisterActivity.class);
                startActivity(intentRegister);
                break;
            case R.id.btn_masuk:
//              Intent intentLogin = new Intent(this, Personal.class);
                progressDialog = ProgressDialog.show(this,null,"Mohon menunggu..",true,false);
                login();
//                startActivity(intentLogin);
                break;
        }

    }

    private void login(){
        Call<UserResponse> call = loginDataService.login(email.getText().toString(), passw.getText().toString());
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.code() == 200){
                    sharedPrefManager.saveSPString(SharedPrefManager.SP_NAME, response.body().getName());
                    Log.d("ROLE : ", response.body().getRole());
                    sharedPrefManager.saveSPString(SharedPrefManager.SP_ROLE, response.body().getRole());
                    sharedPrefManager.saveSPString(SharedPrefManager.SP_TOKEN, "Bearer " +response.body().getToken());
                    Log.d("TOKEN : ", response.body().getToken());
                    Log.d("TOKEN SHARED : ", sharedPrefManager.getSPToken());
                    sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_IS_LOGIN, true);

                    if(response.body().getRole().equals("personal")) {
                        Intent intentLogin = new Intent(LoginActivity.this, Personal.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLogin);
                        finish();
                    }else if(response.body().getRole().equals("community")) {
                        Intent intentLogin = new Intent(LoginActivity.this, Community.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLogin);
                        finish();
                    }
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(LoginActivity.this,"Email/Password salah!!", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d("ERROR LOGIN : ", t.toString());
            }
        });

    }

}
