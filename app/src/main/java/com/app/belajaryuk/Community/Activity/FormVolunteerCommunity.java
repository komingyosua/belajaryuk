package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormVolunteerCommunity extends AppCompatActivity implements View.OnClickListener{

    private static int RESULT_LOAD_IMAGE = 1;
    TextView title_page;
    EditText name;
    EditText description;
    EditText date;
    EditText time;
    EditText place;
    EditText quota;
    ImageView image;
    Button submitVolunteer, editVolunteer;

    String type;

    private ArrayList<Volunteer> volunteer;
    private VolunteerDataService service;
    private Integer volunteerId;
    private String id;
    private String token;

    String imagePath, editImageVolunteer;
    Uri selectedImage;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_volunteer);

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(FormVolunteerCommunity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN Volunteer : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(VolunteerDataService.class);

        title_page = findViewById(R.id.titleFormVolunteer);
        name = findViewById(R.id.titleVolunteer);
        description = findViewById(R.id.descriptionVolunteer);
        date = findViewById(R.id.dateVolunteer);
        time = findViewById(R.id.timeVolunteer);
        place = findViewById(R.id.placeVolunteer);
        quota = findViewById(R.id.quotaVolunteer);
        image = findViewById(R.id.imageVolunteer);
        submitVolunteer = findViewById(R.id.buttonAddVolunteer);
        editVolunteer = findViewById(R.id.buttonEditVolunteer);

        type = getIntent().getStringExtra("type");

        if(getIntent().getParcelableArrayListExtra("dataVolunteer") != null || type.equals("edit")){
            volunteer = getIntent().getParcelableArrayListExtra("dataVolunteer");
            volunteerId = getIntent().getIntExtra("index",0);
            id = getIntent().getStringExtra("volunteerId");

            name.setText(volunteer.get(volunteerId).getTitle());
            description.setText(volunteer.get(volunteerId).getDesc());
            date.setText(volunteer.get(volunteerId).getDate());
            time.setText(volunteer.get(volunteerId).getTime());
            place.setText(volunteer.get(volunteerId).getPlace());
            quota.setText(volunteer.get(volunteerId).getQuota());

            Glide.with(this).load(Endpoint.API_URL +
                    Endpoint.API_READ_EVENT +
                    Endpoint.API_PICTURE +
                    volunteer.get(volunteerId).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(image);
            title_page.setText("Ubah Aktivitas Relawan");
            editVolunteer.setVisibility(View.VISIBLE);
            submitVolunteer.setVisibility(View.GONE);
        }

        image.setOnClickListener(this);
        submitVolunteer.setOnClickListener(this);
        editVolunteer.setOnClickListener(this);
        date.setOnClickListener(this);
        time.setOnClickListener(this);

        /* statusbar init */
        StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageVolunteer:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);
                break;
            case R.id.buttonAddVolunteer:
                File file = new File(imagePath);

                RequestBody title_data = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_data = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_data = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_data = RequestBody.create(time.getText().toString(),MediaType.parse("text/plain"));
                RequestBody place_data = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));
                RequestBody quota_data = RequestBody.create(quota.getText().toString(),MediaType.parse("text/plain"));

                RequestBody requestFile = RequestBody.create(file, MediaType.parse("image/*"));
                MultipartBody.Part body = MultipartBody.Part.createFormData("picture", file.getName(), requestFile);


                Call<BaseResponse<List<Volunteer>>> call = service.apiCreate(token, title_data, desc_data, date_data, time_data, place_data, quota_data, body);
                call.enqueue(new Callback<BaseResponse<List<Volunteer>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Volunteer>>> call, Response<BaseResponse<List<Volunteer>>> response) {
                        if (response.code() == 200) {
                            Intent fav = new Intent(FormVolunteerCommunity.this, RelawanCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Volunteer>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });
                Toast.makeText(FormVolunteerCommunity.this, "Berhasil menambahkan data!", Toast.LENGTH_LONG).show();
                Intent fav = new Intent(FormVolunteerCommunity.this, RelawanCommunity.class);
                startActivity(fav);
                finish();
                break;

            case R.id.buttonEditVolunteer:
                File files = new File(editImageVolunteer);

                RequestBody title_update = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_data2 = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_data2 = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_data2 = RequestBody.create(time.getText().toString(),MediaType.parse("text/plain"));
                RequestBody place_data2 = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));
                RequestBody quota_data2 = RequestBody.create(quota.getText().toString(),MediaType.parse("text/plain"));

                RequestBody requestFile2 = RequestBody.create(files, MediaType.parse("image/*"));
                MultipartBody.Part body2 = MultipartBody.Part.createFormData("picture", files.getName(), requestFile2);


                Call<BaseResponse<List<Volunteer>>> calls = service.apiUpdate(token, id, title_update, desc_data2, date_data2, time_data2, place_data2, quota_data2, body2);
                calls.enqueue(new Callback<BaseResponse<List<Volunteer>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Volunteer>>> call, Response<BaseResponse<List<Volunteer>>> response) {
                        if (response.code() == 200) {
                            Intent fav = new Intent(FormVolunteerCommunity.this, RelawanCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Volunteer>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });
                Toast.makeText(FormVolunteerCommunity.this, "Berhasil mengubah data!", Toast.LENGTH_LONG).show();
                Intent s = new Intent(FormVolunteerCommunity.this, RelawanCommunity.class);
                startActivity(s);
                finish();
                break;
            case R.id.dateVolunteer:
                Calendar newCalendar = Calendar.getInstance();



                DatePickerDialog datePickerDialog = new DatePickerDialog(FormVolunteerCommunity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        date.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.timeVolunteer:
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FormVolunteerCommunity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                // Get the Image from data
                selectedImage = data.getData();
                String selectedFilePath = selectedImage.getPath();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                imagePath = imgDecodableString;
                if(sharedPrefManager.getSpUploadedVolunteer().equals("")) {
                    image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                }else{
                    editImageVolunteer = sharedPrefManager.getSpUploadedVolunteer();
                    image.setImageBitmap(BitmapFactory.decodeFile(sharedPrefManager.getSpUploadedVolunteer()));
                }
                sharedPrefManager.saveSPString("SP_UPLOADED_VOLUNTEER", imgDecodableString);

                Log.d("GAMBARNYA : ", selectedFilePath);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }
}
