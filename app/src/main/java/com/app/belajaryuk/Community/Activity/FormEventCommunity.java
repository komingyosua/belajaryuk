package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FormEventCommunity extends AppCompatActivity implements View.OnClickListener{

    private static int RESULT_LOAD_IMAGE = 1;
    TextView title_page;
    EditText name;
    EditText description;
    EditText date;
    EditText time;
    EditText place;
    ImageView image;
    Button submitEvent, editEvent;
    String type;
    private ArrayList<Event> event;
    private EventDataService service;
    private String id;
    private Integer eventId;
    private String token;
    Bitmap bitmap;

    String imagePath;
    Uri selectedImage;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_event);

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(FormEventCommunity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
        Log.d("TOKEN EVENT : ", sharedPrefManager.getSPUploadedEvent());
        service = ServiceGenerator.getClient().create(EventDataService.class);

        title_page = findViewById(R.id.titleFormEvent);
        name = findViewById(R.id.titleEvent);
        description = findViewById(R.id.descriptionEvent);
        date = findViewById(R.id.dateEvent);
        time = findViewById(R.id.timeEvent);
        place = findViewById(R.id.placeEvent);
        image = findViewById(R.id.imageEvent);
        submitEvent = findViewById(R.id.buttonAddEvent);
        editEvent = findViewById(R.id.buttonEditEvent);

        type = getIntent().getStringExtra("type");
        if(getIntent().getParcelableArrayListExtra("data") != null || type.equals("edit")){
            event = getIntent().getParcelableArrayListExtra("data");
            id = getIntent().getStringExtra("id");
            eventId = getIntent().getIntExtra("index", 0);

            name.setText(event.get(eventId).getTitle());
            description.setText(event.get(eventId).getDesc());
            date.setText(event.get(eventId).getDate());
            time.setText(event.get(eventId).getTime());
            place.setText(event.get(eventId).getPlace());

            /*Glide.with(this).load(Endpoint.API_URL +
                    Endpoint.API_READ_EVENT +
                    Endpoint.API_PICTURE +
                    event.get(eventId).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(image);*/
            title_page.setText("Ubah Aktivitas");
            editEvent.setVisibility(View.VISIBLE);
            submitEvent.setVisibility(View.GONE);
        }

        image.setOnClickListener(this);
        submitEvent.setOnClickListener(this);
        editEvent.setOnClickListener(this);
        date.setOnClickListener(this);
        time.setOnClickListener(this);

        /* Statusbar init */
        StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor), 0);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageEvent:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);
                break;
            case R.id.buttonAddEvent:

                File file = new File(imagePath);


                RequestBody title_data = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_data = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_data = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_data = RequestBody.create(time.getText().toString(),MediaType.parse("text/plain"));
                RequestBody place_data = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));

                RequestBody requestFile = RequestBody.create(file, MediaType.parse("image/*"));
                MultipartBody.Part body = MultipartBody.Part.createFormData("picture", file.getName(), requestFile);


                Call<BaseResponse<List<Event>>> call = service.apiCreate(token, title_data, desc_data, date_data, time_data, place_data, body);
                call.enqueue(new Callback<BaseResponse<List<Event>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                        if (response.code() == 200) {
                            Intent fav = new Intent(FormEventCommunity.this, AktivitasCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });

                Toast.makeText(FormEventCommunity.this, "Berhasil menambahkan data!", Toast.LENGTH_LONG).show();
                finish();
                break;
            case R.id.buttonEditEvent:
                    File files = new File(imagePath);
                    RequestBody requestFileUpdate = RequestBody.create(files, MediaType.parse("image/*"));
                    MultipartBody.Part body_update = MultipartBody.Part.createFormData("picture", files.getName(), requestFileUpdate);


                RequestBody title_update = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_update = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_update = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_update = RequestBody.create(time.getText().toString(),MediaType.parse("text/plain"));
                RequestBody place_update = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));




                Call<BaseResponse<List<Event>>> calls = service.apiUpdate(token, id, title_update, desc_update, date_update, time_update, place_update, body_update);
                calls.enqueue(new Callback<BaseResponse<List<Event>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                        if (response.code() == 200) {
                            Intent fav = new Intent(FormEventCommunity.this, AktivitasCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });

                Toast.makeText(FormEventCommunity.this, "Berhasil mengubah data!", Toast.LENGTH_LONG).show();
                finish();
                break;
            case R.id.dateEvent:
                Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(FormEventCommunity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        date.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.timeEvent:
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FormEventCommunity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                    // Get the Image from data
                    selectedImage = data.getData();
                    String selectedFilePath = selectedImage.getPath();

                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    //Get the column index of MediaStore.Images.Media.DATA
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    //Gets the String value in the column
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    imagePath = imgDecodableString;
                    if(sharedPrefManager.getSPUploadedEvent().equals("")) {
                        image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                    }else{
                        image.setImageBitmap(BitmapFactory.decodeFile(sharedPrefManager.getSPUploadedEvent()));
                    }
                    sharedPrefManager.saveSPString("SP_UPLOADED_EVENT", imgDecodableString);

                    Log.d("GAMBARNYA : ", selectedFilePath);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }
}
