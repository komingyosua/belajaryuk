package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.app.belajaryuk.Adapter.VolunteerAdapterCommunity;
import com.app.belajaryuk.Data.Volunteer;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.VolunteerDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RelawanCommunity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = RelawanCommunity.class.getSimpleName();
  RecyclerView rv_volunteer;
  LinearLayout not_found, btn_bottom_create;
  Button createVolunteer, createVolunteerBtm;
  private String token;
  private ArrayList<Volunteer> data;
  private VolunteerAdapterCommunity volAdapter;
  private VolunteerDataService service;
  SharedPrefManager sharedPrefManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_relawan_community);

    sharedPrefManager = new SharedPrefManager(this);
    token = sharedPrefManager.getSPToken();
    Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
    service = ServiceGenerator.getClient().create(VolunteerDataService.class);

    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    /* Resource initialization */
    rv_volunteer = findViewById(R.id.rv_volunteer_community);
    not_found = findViewById(R.id.volunteer_empty_community);
    btn_bottom_create = findViewById(R.id.btn_bottom_create_volunteer_community);
    createVolunteer = findViewById(R.id.btn_create_volunteer_community);
    createVolunteerBtm = findViewById(R.id.btn_createform_volunteer_community);
    createVolunteerBtm.setOnClickListener(this);
    createVolunteer.setOnClickListener(this);

    /*Recycleview Volunteer*/

      loadData();
      rv_volunteer = findViewById(R.id.rv_volunteer_community);
      volAdapter = new VolunteerAdapterCommunity(this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      rv_volunteer.setLayoutManager(layoutManager);
      rv_volunteer.setAdapter(volAdapter);


  }

  @Override
  public void onClick(View view) {
    if(view.getId() == R.id.btn_create_volunteer_community || view.getId() == R.id.btn_createform_volunteer_community){
      Intent createVolunteer = new Intent(this, FormVolunteerCommunity.class);
      createVolunteer.putExtra("type", "create");
      startActivity(createVolunteer);
    }
  }

  private void loadData() {
    Call<BaseResponse<List<Volunteer>>> call = service.apiRead(token);
    call.enqueue(new Callback<BaseResponse<List<Volunteer>>>() {
      @Override
      public void onResponse(Call<BaseResponse<List<Volunteer>>> call, Response<BaseResponse<List<Volunteer>>> response) {
        if (response.code() == 200) {
          if (response.body().getData() != null) {
            Log.d("DATA : ", response.body().getData().toString());
            volAdapter.addAll(response.body().getData());
          } else {
            Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
            Log.d("MESSAGE : ", response.body().getMessage());
          }
        }
      }

      @Override
      public void onFailure(Call<BaseResponse<List<Volunteer>>> call, Throwable t) {
        Log.e(TAG+".error", t.toString());
      }
    });
  }

  void addDataVolunteer(){
//    data = new ArrayList<>();
//    data.add(new com.farhan.slider.Data.Volunteer(0,"10 / 100 Peserta","Relawan Pengajar Sasindo27", "12 Agustus 2019 09:00", "yuk... buat kalian yang menyukai dunia anak silahkan bergabung di komunitas kami ,komunitas sasindo27 adalah komunitas yang berbasis pendidikan yang awalnya menjadi dunia literasi dan kini akan dikembangkan untuk pengembangan anak muda indonesia.", "Palmerah, Jakarta Barat", "https://www.indorelawan.org/uploads/gallery/2019-8-13/5d7ba731686ae5e9b0d13767_thumbnail370x250.JPG"));
//    data.add(new com.farhan.slider.Data.Volunteer(1,"28 / 100 Peserta","English Teacher for SD", "8 Juli 2019, 11:00","","Palmerah, Jakarta Barat", "https://www.indorelawan.org/uploads/gallery/2019-8-10/5d777acc02583aa3a49d9e01_thumbnail370x250.jpg"));
//    data.add(new com.farhan.slider.Data.Volunteer(
//     2,
//     "0 / 100 Peserta",
//     "Relawan mengajar",
//      "6 Oktober 2019, 09:00",
//     "Yuk kita mengajar",
//     "Kebon Jeruk, Jakarta Barat",
//     "http://pd.pps.uny.ac.id/sites/pd.pps.uny.ac.id/files/field/image/P_20151030_081230_HDR.jpg"));

  }

  @Override
  public void onResume() {
    super.onResume();
    volAdapter = new VolunteerAdapterCommunity(this);
    rv_volunteer.setAdapter(volAdapter);
  }
}
