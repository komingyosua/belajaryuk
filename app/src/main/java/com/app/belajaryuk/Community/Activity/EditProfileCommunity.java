package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Community;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.CommunityDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileCommunity extends AppCompatActivity implements View.OnClickListener {
    private static int RESULT_LOAD_IMAGE = 1;
    LinearLayout ll_community;
    EditText phone, address, city, province, postcode;
    TextView hint;
    ImageButton back;
    ImageView imageEditProfile;
    Button update;
    private ArrayList<Community> currentCommunity = new ArrayList<>();
    SharedPrefManager sharedPrefManager;
    private String token;
    ProgressDialog progressDialog;
    CommunityDataService communityDataService;
    String imagePath;
    Uri selectedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_community);


        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EditProfileCommunity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        sharedPrefManager = new SharedPrefManager(this);

        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN COMMUNITY PROF : ", token);
        communityDataService = ServiceGenerator.getClient().create(CommunityDataService.class);
        currentCommunity = getIntent().getParcelableArrayListExtra("data");

        ll_community = findViewById(R.id.containerFormEditProfileCommunity);
        phone = findViewById(R.id.et_phoneNumberProfileCommunity);
        address = findViewById(R.id.et_addressProfileCommunity);
        city = findViewById(R.id.et_cityProfileCommunity);
        province = findViewById(R.id.et_provinceProfileCommunity);
        postcode = findViewById(R.id.et_postcodeProfileCommunity);
        hint = findViewById(R.id.hintTextEditProfileCommunity);
        back = findViewById(R.id.backEditProfileCommunity);
        imageEditProfile = findViewById(R.id.imageEditProfileCommunity);
        update = findViewById(R.id.buttonUpdateProfileCommunity);
        update.setOnClickListener(this);
        back.setOnClickListener(this);
        imageEditProfile.setOnClickListener(this);


        phone.setText(currentCommunity.get(0).getPhone());
        address.setText(currentCommunity.get(0).getAddress());
        city.setText(currentCommunity.get(0).getCity());
        province.setText(currentCommunity.get(0).getProvince());
        postcode.setText(currentCommunity.get(0).getPost_code());
        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_PERSONAL +
                Endpoint.API_PICTURE +
                currentCommunity.get(0).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imageEditProfile);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonUpdateProfileCommunity:
                progressDialog.setMessage("Mohon Menunggu..");
                progressDialog.show();
                File files = new File(imagePath);

                RequestBody phone_update = RequestBody.create(phone.getText().toString(), MediaType.parse("text/plain"));
                RequestBody address_update = RequestBody.create(address.getText().toString(), MediaType.parse("text/plain"));
                RequestBody city_update = RequestBody.create(city.getText().toString(), MediaType.parse("text/plain"));
                RequestBody province_update = RequestBody.create(province.getText().toString(), MediaType.parse("text/plain"));
                RequestBody postcode_update = RequestBody.create(postcode.getText().toString(), MediaType.parse("text/plain"));
                RequestBody requestFileImg = RequestBody.create(files, MediaType.parse("image/*"));
                MultipartBody.Part body2 = MultipartBody.Part.createFormData("picture", files.getName(), requestFileImg);

                Call<BaseResponse<Community>> call = communityDataService.updateCommunityData(token,phone_update,postcode_update,province_update,city_update,address_update,body2);
                call.enqueue(new Callback<BaseResponse<Community>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Community>> call, Response<BaseResponse<Community>> response) {
                        if(response.code() == 200){
                            Toast.makeText(EditProfileCommunity.this, "Berhasil update profile komunitas!", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            Log.d("Error brodie:", response.errorBody().toString());
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Community>> call, Throwable t) {
                        Toast.makeText(EditProfileCommunity.this, "Gagal", Toast.LENGTH_SHORT).show();
                        Log.d("ERROR : ", t.toString());
                    }
                });

                break;

            case R.id.imageEditProfileCommunity:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);
                break;

            case R.id.backEditProfileCommunity:
                onBackPressed();
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                // Get the Image from data
                selectedImage = data.getData();
                String selectedFilePath = selectedImage.getPath();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                imagePath = imgDecodableString;
                imageEditProfile.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

                Log.d("GAMBARNYA : ", selectedFilePath);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }
}
