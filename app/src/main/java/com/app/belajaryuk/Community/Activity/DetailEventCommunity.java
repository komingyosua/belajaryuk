package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.ImagePopup;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.Utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailEventCommunity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    ImageView imgBanner;
    private LinearLayout items_layout;
    private String type;
    private boolean isHideToolbarView = false;
    private FrameLayout date_behavior;
    private LinearLayout titleAppbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private TextView appbar_title, category, time, title, content, place;
    private ArrayList<Event> events = new ArrayList<>();
    private EventDataService service;
    private int eventId;
    private String id;
    private String token;
    Button btnEdit, btnDelete;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_detail_event_community);

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN EVENT DETAIL : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(EventDataService.class);

        events = getIntent().getParcelableArrayListExtra("dataEvent");
        type = getIntent().getStringExtra("type");
        eventId = getIntent().getIntExtra("index", 0);
        id = getIntent().getStringExtra("id");
        Log.d("ID EVENT : ", id);

        toolbar = findViewById(R.id.toolbar_event_community);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_event_community);
        collapsingToolbarLayout.setTitle("");
        Toolbar toolbar = findViewById(R.id.toolbar_event_community);
        setSupportActionBar(toolbar);


        imgBanner = findViewById(R.id.image_detail_event_community);
        date_behavior = findViewById(R.id.date_behavior_event_community);
        titleAppbar = findViewById(R.id.title_appbar_event_community);
        appBarLayout = findViewById(R.id.appbar_event_community);
        appBarLayout.addOnOffsetChangedListener(this);
        appbar_title = findViewById(R.id.title_on_appbar_event_community);
        category = findViewById(R.id.category_detail_event_community);
        time = findViewById(R.id.time_detail_event_community);
        title = findViewById(R.id.title_detail_event_community);
        content = findViewById(R.id.content_detail_event_community);

        btnDelete = findViewById(R.id.btnDeleteEventCommunity);
        btnEdit = findViewById(R.id.btnUpdateEventCommunity);
        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        Glide.with(this).load(Endpoint.API_URL +
                Endpoint.API_READ_EVENT +
                Endpoint.API_PICTURE +
                events.get(eventId).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(imgBanner);
        imagePopup.initiatePopupWithGlide(events.get(eventId).getPicture());


        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopupWithGlide(Endpoint.API_URL +
                        Endpoint.API_READ_EVENT +
                        Endpoint.API_PICTURE +
                        events.get(eventId).getPicture());
                imagePopup.viewPopup();
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());

        appbar_title.setText(events.get(eventId).getTitle()); //fill
        title.setText(events.get(eventId).getTitle()); //fill
        //2019-08-22T14:00:32Z
        time.setText(events.get(eventId).getDate());
        content.setText(events.get(eventId).getDesc());
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorPrimary));

        Log.d("EVENT : ", events.get(eventId).toString());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnUpdateEventCommunity:
                //TODO LOGIC EDIT
                Intent edit = new Intent(this, FormEventCommunity.class);
                edit.putParcelableArrayListExtra("data", events);
                edit.putExtra("id", events.get(eventId).getId());
                edit.putExtra("eventId", eventId);
                edit.putExtra("type", "edit");
                startActivity(edit);
                finish();
                break;
            case R.id.btnDeleteEventCommunity:
                //TODO DELETE
                delete();
                onBackPressed();
                finish();
                break;
        }
    }

    void delete(){
        Call<BaseResponse<List<Event>>> call = service.deleteEvent(token,id);
        call.enqueue(new Callback<BaseResponse<List<Event>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
                if(response.code() == 200){
                    Toast.makeText(DetailEventCommunity.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            date_behavior.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            date_behavior.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }
}
