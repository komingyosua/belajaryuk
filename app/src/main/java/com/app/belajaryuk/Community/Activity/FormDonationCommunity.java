package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Donation;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormDonationCommunity extends AppCompatActivity implements View.OnClickListener {

    private static int RESULT_LOAD_IMAGE = 1;
    TextView title_page;
    EditText name;
    EditText description;
    EditText date;
    EditText place;
    EditText pool;
    EditText creator;
    EditText time;
    RadioGroup categoryGroup;
    RadioButton categoryUang;
    RadioButton categoryBarang;
    ImageView image;
    Button submitDonation, updateDonation;

    String type;
    private ArrayList<Donation> donation;
    private DonationDataService service;
    private Integer donationId;
    private String id;
    private String token;

    String imagePath;
    String category_value;
    Uri selectedImage;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_donation);

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(FormDonationCommunity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN Volunteer : ", sharedPrefManager.getSPToken());
        service = ServiceGenerator.getClient().create(DonationDataService.class);

        title_page = findViewById(R.id.titleFormDonation);
        name = findViewById(R.id.titleDonation);
        description = findViewById(R.id.descriptionDonation);
        date = findViewById(R.id.dateDonation);
        place = findViewById(R.id.placeDonation);
        pool = findViewById(R.id.poolDonation);
        time = findViewById(R.id.timeDonation);
        image = findViewById(R.id.imageDonation);
        creator = findViewById(R.id.creator_donation);
        submitDonation = findViewById(R.id.buttonAddDonation);
        categoryGroup = findViewById(R.id.groupCategory);
        categoryUang = findViewById(R.id.radioUang);
        categoryBarang = findViewById(R.id.radioBarang);
        updateDonation = findViewById(R.id.buttonUpdateDonation);


        type = getIntent().getStringExtra("type");

        if(getIntent().getParcelableArrayListExtra("donations") != null || type.equals("edit")){
            donation = getIntent().getParcelableArrayListExtra("donations");
            donationId = getIntent().getIntExtra("index",0);
            id = getIntent().getStringExtra("id");

            name.setText(donation.get(donationId).getTitle());
            description.setText(donation.get(donationId).getDesc());
            date.setText(donation.get(donationId).getDate());
            pool.setText(donation.get(donationId).getPool());
            time.setText(donation.get(donationId).getTime());
            place.setText(donation.get(donationId).getPlace());
            creator.setText(donation.get(donationId).getCreator());

            if (donation.get(donationId).getCategory().equals(categoryUang.getText().toString().toLowerCase())){
                categoryUang.setChecked(true);
                categoryBarang.setChecked(false);
            } else {
                categoryUang.setChecked(false);
                categoryBarang.setChecked(true);
            }

            Glide.with(this).load(Endpoint.API_URL +
                    Endpoint.API_READ_DONATION +
                    Endpoint.API_PICTURE +
                    donation.get(donationId).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(image);
            title_page.setText("Ubah Aktivitas Relawan");
            updateDonation.setVisibility(View.VISIBLE);
            submitDonation.setVisibility(View.GONE);
        }

        image.setOnClickListener(this);
        updateDonation.setOnClickListener(this);
        submitDonation.setOnClickListener(this);
        date.setOnClickListener(this);
        time.setOnClickListener(this);

        /* statusbar init */
        StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageDonation:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);
                break;
            case R.id.buttonAddDonation:
                File files = new File(imagePath);

                RequestBody title_data = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_data = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_data = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody pool_data = RequestBody.create(pool.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_data = RequestBody.create(time.getText().toString(), MediaType.parse("text/plain"));
                RequestBody place_data = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));
                RequestBody creator_data = RequestBody.create(creator.getText().toString(),MediaType.parse("text/plain"));
                int selectedId = categoryGroup.getCheckedRadioButtonId();

                if(selectedId == categoryUang.getId()){
                    category_value = categoryUang.getText().toString().toLowerCase();
                }else if(selectedId == categoryBarang.getId()){
                    category_value = categoryBarang.getText().toString().toLowerCase();
                }
                RequestBody category_data = RequestBody.create(category_value,MediaType.parse("text/plain"));

                RequestBody requestFile = RequestBody.create(files, MediaType.parse("image/*"));
                MultipartBody.Part body = MultipartBody.Part.createFormData("picture", files.getName(), requestFile);


                Call<BaseResponse<List<Donation>>> call = service.apiCreate(token, title_data, desc_data, date_data, time_data, place_data, pool_data, creator_data, category_data, body);
                call.enqueue(new Callback<BaseResponse<List<Donation>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Donation>>> call, Response<BaseResponse<List<Donation>>> response) {
                        if (response.code() == 200) {
                            Log.d("Errornya:", response.errorBody().toString());
                            Intent fav = new Intent(FormDonationCommunity.this, DonationCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Donation>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });
                Toast.makeText(FormDonationCommunity.this, "Berhasil mengubah data!", Toast.LENGTH_LONG).show();
                Intent s = new Intent(FormDonationCommunity.this, DonationCommunity.class);
                startActivity(s);
                finish();
                break;
            case R.id.buttonUpdateDonation:
                File file = new File(imagePath);

                RequestBody title_update = RequestBody.create(name.getText().toString(), MediaType.parse("text/plain"));
                RequestBody desc_data2 = RequestBody.create(description.getText().toString(),MediaType.parse("text/plain"));
                RequestBody date_data2 = RequestBody.create(date.getText().toString(),MediaType.parse("text/plain"));
                RequestBody pool_data2 = RequestBody.create(pool.getText().toString(),MediaType.parse("text/plain"));
                RequestBody time_data2 = RequestBody.create(time.getText().toString(), MediaType.parse("text/plain"));
                RequestBody place_data2 = RequestBody.create(place.getText().toString(),MediaType.parse("text/plain"));
                RequestBody creator_data2 = RequestBody.create(creator.getText().toString(),MediaType.parse("text/plain"));

                int selectedIds = categoryGroup.getCheckedRadioButtonId();

                if(selectedIds == categoryUang.getId()){
                    category_value = categoryUang.getText().toString().toLowerCase();
                }else if(selectedIds == categoryBarang.getId()){
                    category_value = categoryBarang.getText().toString().toLowerCase();
                }
                RequestBody category_data2 = RequestBody.create(category_value,MediaType.parse("text/plain"));

                RequestBody requestFile2 = RequestBody.create(file, MediaType.parse("image/*"));
                MultipartBody.Part body2 = MultipartBody.Part.createFormData("picture", file.getName(), requestFile2);


                Call<BaseResponse<List<Donation>>> calls = service.apiUpdate(token, id, title_update, desc_data2, date_data2, time_data2, place_data2, pool_data2, creator_data2, category_data2, body2);
                calls.enqueue(new Callback<BaseResponse<List<Donation>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Donation>>> call, Response<BaseResponse<List<Donation>>> response) {
                        if (response.code() == 200) {
                            Log.d("Errornya:", response.errorBody().toString());
                            Intent fav = new Intent(FormDonationCommunity.this, DonationCommunity.class);
                            startActivity(fav);
                            finish();
                        }
                    }

                    public void onFailure(Call<BaseResponse<List<Donation>>> call, Throwable t) {
                        Log.d("Errornya", t.toString());
                    }
                });
                Toast.makeText(FormDonationCommunity.this, "Berhasil mengubah data!", Toast.LENGTH_LONG).show();
                Intent j = new Intent(FormDonationCommunity.this, DonationCommunity.class);
                startActivity(j);
                finish();
                break;
            case R.id.dateDonation:
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(FormDonationCommunity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        date.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.timeDonation:
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FormDonationCommunity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();
                break;
        }
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                // Get the Image from data
                selectedImage = data.getData();
                String selectedFilePath = selectedImage.getPath();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                imagePath = imgDecodableString;
                image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

                Log.d("GAMBARNYA : ", selectedFilePath);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }
}
