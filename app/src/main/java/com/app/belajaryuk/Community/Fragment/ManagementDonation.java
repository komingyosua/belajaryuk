package com.app.belajaryuk.Community.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.belajaryuk.Adapter.DonationApprovalAdapter;
import com.app.belajaryuk.Data.DonationApproval;
import com.app.belajaryuk.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementDonation extends Fragment {
   ArrayList<DonationApproval> dataList;
   DonationApprovalAdapter donationApprovalAdapter;
  RecyclerView rv_donation;

  public ManagementDonation() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    addDataDonation();
    View view =  inflater.inflate(R.layout.fragment_management_donation, container, false);
    rv_donation = view.findViewById(R.id.rv_list_of_donation);
    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    donationApprovalAdapter = new DonationApprovalAdapter(dataList,getContext());
    rv_donation.setLayoutManager(linearLayoutManager);
    rv_donation.setAdapter(donationApprovalAdapter);
    return view;
  }

  void addDataDonation(){
    dataList = new ArrayList<>();
    dataList.add(new DonationApproval(0, "Koming Yosua","100 Buku untuk Anak kurang Mampu","Uang" , "MENUNGGU PENGIRIMAN", "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"));

  }


}
