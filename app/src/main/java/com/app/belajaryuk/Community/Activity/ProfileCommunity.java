package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.Community;
import com.app.belajaryuk.Network.Endpoint;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.CommunityDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.app.belajaryuk.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileCommunity extends AppCompatActivity implements View.OnClickListener {
    CircleImageView profilepict;
    TextView name, editProfile, editPassword, logout, role;
    SharedPrefManager sharedPrefManager;
    private ArrayList<Community> currentCommunity = new ArrayList<>();
    private String token;
    CommunityDataService communityDataService;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_community);
        profilepict = findViewById(R.id.profileImgViewCommunity);
        name = findViewById(R.id.profileNameCommunity);
        editProfile = findViewById(R.id.tv_editProfileCommunity);
        editProfile.setOnClickListener(this);
        editPassword = findViewById(R.id.tv_editPasswordCommunity);
        editPassword.setOnClickListener(this);
        logout = findViewById(R.id.tv_logoutCommunity);
        logout.setOnClickListener(this);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        Log.d("TOKEN ASSIGNED : ", token);
        communityDataService = ServiceGenerator.getClient().create(CommunityDataService.class);


        loadCommunityData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_editProfileCommunity:
                Intent edit = new Intent(this, EditProfileCommunity.class);
                edit.putExtra("data", currentCommunity);
                Log.d("ID : ", String.valueOf(currentCommunity.get(0).getIndex()));
                startActivity(edit);
                break;

            case R.id.tv_editPassword:
                Intent editPass = new Intent(this, EditPasswordCommunity.class);
                startActivity(editPass);
                break;

            case R.id.tv_logoutCommunity:
                sharedPrefManager.logout();
                Intent logout = new Intent(this, WelcomeActivity.class);
                startActivity(logout);
                Toast.makeText(this, "Logout Berhasil!", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }

    }


    void loadCommunityData(){
        Call<BaseResponse<List<Community>>> call = communityDataService.apiRead(token);
        call.enqueue(new Callback<BaseResponse<List<Community>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Community>>> call, Response<BaseResponse<List<Community>>> response) {
                if(response.code() == 200){
                    Glide.with(ProfileCommunity.this).load(Endpoint.API_URL +
                            Endpoint.API_COMMUNITY +
                            Endpoint.API_PICTURE +
                            response.body().getData().get(0).getPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(profilepict);
                    name.setText(response.body().getData().get(0).getName());
                    currentCommunity.add(response.body().getData().get(0));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Community>>> call, Throwable t) {
                Log.d("ERROR : ", t.toString());
            }
        });
    }
}
