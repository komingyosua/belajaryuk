package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.app.belajaryuk.Adapter.EventCommunityAdapter;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.R;

import java.util.ArrayList;

public class ListEventComunnity extends AppCompatActivity {

    RecyclerView recycler_activity;
    private ArrayList<Event> data;
    private EventCommunityAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event_comunnity);

        ImageButton back = findViewById(R.id.backToMainListEvent);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addData();
        recycler_activity = findViewById(R.id.recyclerListEvent);
        eventAdapter = new EventCommunityAdapter(data,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ListEventComunnity.this);
        recycler_activity.setLayoutManager(layoutManager);
        recycler_activity.setAdapter(eventAdapter);
    }

    void addData(){
//        data = new ArrayList<>();
//        data.add(new Event(0,"Belajar Matematika Kelas SMP", "content", "Palmerah, Jakarta Barat", "5 Oktober 2019 09:00", "https://www.indorelawan.org/uploads/gallery/2019-8-16/5d7f2d64136a329473937199_thumbnail370x250.jpg"));
//        data.add(new Event(1,"Membuat Blog dan Menulis Artikel","content","Duren Sawit, Jakarta Timur", "15 September 2019 11:00", "https://i0.wp.com/joglosemarnews.com/images/2019/05/290519sd-pk-kottabarat.jpeg?fit=1040%2C780&ssl=1"));
//        data.add(new Event(2,"Pendidikan Karakter","content","Kelapa Dua, Jakarta Barat", "18 Agustus 2019 09:00", "http://humaniora.uin-malang.ac.id/blog/berita/wp-content/uploads/sites/3/2014/09/er1123.jpg"));
//        data.add(new Event(3,"TEDxUbud 2019: Movement","content","Setia Darma House", "Sabtu, Agustus 12, 09:00 AM", "https://quex-indonesia.com/wp-content/uploads/2019/06/Event-PRJ-2019.png"));
    }
}
