package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.app.belajaryuk.Data.Community;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.CommunityDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPasswordCommunity extends AppCompatActivity {
    ImageButton back;
    EditText old, newpass, confirmpass;
    Button submit;
    private String token;
    CommunityDataService communityDataService;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password_community);

        sharedPrefManager = new SharedPrefManager(EditPasswordCommunity.this);
        token = sharedPrefManager.getSPToken();
        communityDataService = ServiceGenerator.getClient().create(CommunityDataService.class);

        back = findViewById(R.id.backEditPassProfileCommunity);
        old = findViewById(R.id.et_OldPassCommunity);
        newpass = findViewById(R.id.et_newPassCommunity);
        confirmpass = findViewById(R.id.et_confirmPassCommunity);
        submit = findViewById(R.id.buttonUpdatePassProfileCommunity);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.buttonUpdatePassProfileCommunity){
                    submitChangePassword();
                }
            }
        });
    }

    void submitChangePassword(){
        Call<BaseResponse<List<Community>>> call = communityDataService.changePassword(token, old.getText().toString(), newpass.getText().toString(), confirmpass.getText().toString());
        call.enqueue(new Callback<BaseResponse<List<Community>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Community>>> call, Response<BaseResponse<List<Community>>> response) {
                if(response.code() == 200){
                    Toast.makeText(EditPasswordCommunity.this, "Password berhasil diubah!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Community>>> call, Throwable t) {
                Log.d("ERROR FAILURE : ", t.toString());
            }
        });
    }
}
