package com.app.belajaryuk.Community.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.app.belajaryuk.Community.PointsOverlayView;
import com.app.belajaryuk.R;
import com.google.android.material.snackbar.Snackbar;
import com.jaeger.library.StatusBarUtil;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ScanQR extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, QRCodeReaderView.OnQRCodeReadListener {
  private static final int MY_PERMISSION_REQUEST_CAMERA = 0;
  boolean confirmation = false;
  int count = 0;
  private ViewGroup mainLayout;
  private QRCodeReaderView qrCodeReaderView;
  private PointsOverlayView pointsOverlayView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_scan_qr);
    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);
    mainLayout = findViewById(R.id.main_layout);
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED) {
      initQRCodeReaderView();
    } else {
      requestCameraPermission();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (qrCodeReaderView != null) {
      qrCodeReaderView.startCamera();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (qrCodeReaderView != null) {
      qrCodeReaderView.stopCamera();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    qrCodeReaderView.stopCamera();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
      return;
    }

    if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      Snackbar.make(mainLayout, "Camera permission was granted.", Snackbar.LENGTH_SHORT).show();
      initQRCodeReaderView();
    } else {
      Snackbar.make(mainLayout, "Camera permission request was denied.", Snackbar.LENGTH_SHORT)
              .show();
    }
  }

  private void initQRCodeReaderView() {
    View content = getLayoutInflater().inflate(R.layout.content_decoder, mainLayout, true);

    qrCodeReaderView = content.findViewById(R.id.qrdecoderview);
    pointsOverlayView = content.findViewById(R.id.points_overlay_view);

    qrCodeReaderView.setAutofocusInterval(2000L);
    qrCodeReaderView.setOnQRCodeReadListener(this);
    qrCodeReaderView.setBackCamera();
    qrCodeReaderView.startCamera();
  }

  private void requestCameraPermission(){
    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
      Snackbar.make(mainLayout, "Camera access is required to display the camera preview.",
              Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
        @Override public void onClick(View view) {
          ActivityCompat.requestPermissions(ScanQR.this, new String[] {
                  Manifest.permission.CAMERA
          }, MY_PERMISSION_REQUEST_CAMERA);
        }
      }).show();
    } else {
      Snackbar.make(mainLayout, "Permission is not available. Requesting camera permission.",
              Snackbar.LENGTH_SHORT).show();
      ActivityCompat.requestPermissions(this, new String[] {
              Manifest.permission.CAMERA
      }, MY_PERMISSION_REQUEST_CAMERA);
    }
  }


  @Override
  public void onQRCodeRead(String text, PointF[] points) {
    count++;
    qrCodeReaderView.stopCamera();
    if(count<=1) {
      pointsOverlayView.setPoints(points);
      final String result = text;
      new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
              .setTitleText("Verifikasi")
              .setContentText("Absensi kehadiran untuk : " + text)
              .setConfirmText("Ya")
              .setConfirmButtonBackgroundColor(getResources().getColor(R.color.primaryColor))
              .setConfirmButtonTextColor(getResources().getColor(R.color.white))
              .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(final SweetAlertDialog sDialog) {
                  sDialog
                          .setTitleText("Berhasil!")
                          .setContentText("Absensi kehadiran untuk " + result + " telah berhasil!")
                          .setConfirmText("Ok")
                          .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                              sweetAlertDialog.dismissWithAnimation();
                              finish();
                            }
                          })
                          .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                }
              })
              .show();
    }
  }
}
