package com.app.belajaryuk.Community.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.belajaryuk.Adapter.UserAdapter;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementActivity extends Fragment {
  private ArrayList<User> waitingList, approvedList, declinedList;
  private UserAdapter waitingAdapter, approvedAdapter, declinedAdapter;
  RecyclerView rv_waiting, rv_approved, rv_declined;


  public ManagementActivity() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_management, container, false);

    addWaiting();
    rv_waiting = view.findViewById(R.id.rv_waiting_for_approval_activity);
    final LinearLayoutManager layoutManagerWaiting = new LinearLayoutManager(getContext());
    waitingAdapter = new UserAdapter(waitingList, getContext());
    rv_waiting.setLayoutManager(layoutManagerWaiting);
    rv_waiting.setAdapter(waitingAdapter);

    addApproved();
    rv_approved = view.findViewById(R.id.rv_approved_activity);
    final LinearLayoutManager layoutManagerApproved = new LinearLayoutManager(getContext());
    approvedAdapter = new UserAdapter(approvedList, getContext());
    rv_approved.setLayoutManager(layoutManagerApproved);
    rv_approved.setAdapter(approvedAdapter);

    addDeclined();
    rv_declined = view.findViewById(R.id.rv_declined_activity);
    final LinearLayoutManager layoutManagerDeclined = new LinearLayoutManager(getContext());
    declinedAdapter = new UserAdapter(declinedList,getContext());
    rv_declined.setLayoutManager(layoutManagerDeclined);
    rv_declined.setAdapter(declinedAdapter);


    return view;

  }

  void addWaiting(){

    waitingList = new ArrayList<>();
    waitingList.add(new User("0", "Koming Yosua","Belajar Matematika Kelas SMP", "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png","process"));
  }

  void addApproved(){
    approvedList = new ArrayList<>();
  }

  void addDeclined(){
    declinedList = new ArrayList<>();
  }


}
