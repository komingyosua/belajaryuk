package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.app.belajaryuk.Adapter.ManagementPager;
import com.app.belajaryuk.R;
import com.jaeger.library.StatusBarUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

public class UserManagement extends AppCompatActivity {
  private ViewPager viewPager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user_management);

    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    /* Viewpager init */
    viewPager = findViewById(R.id.viewpagerManagement);
    ManagementPager managementPager = new ManagementPager(getSupportFragmentManager());
    viewPager.setAdapter(managementPager);
    SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertabManagement);
    viewPagerTab.setViewPager(viewPager);

  }
}
