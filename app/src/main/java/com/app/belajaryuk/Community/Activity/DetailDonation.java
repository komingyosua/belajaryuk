package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.DonationApproval;
import com.app.belajaryuk.R;

import java.util.ArrayList;
import java.util.List;

public class DetailDonation extends AppCompatActivity implements View.OnClickListener {
  List<DonationApproval> data = new ArrayList<>();
  private int donationId;
  private String id;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    ImageView img;
    TextView name, donationName, type, status;
    Button finish;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_donation2);

    /* resource initialization*/
    img = findViewById(R.id.donation_detail_picture);
    name = findViewById(R.id.donation_detail_name);
    donationName = findViewById(R.id.donation_detail_event);
    type = findViewById(R.id.donation_detail_type);
    status = findViewById(R.id.donation_detail_status);
    finish = findViewById(R.id.btn_finish_detail_donation);
    finish.setOnClickListener(this);

    /* get data from intent*/
    data = getIntent().getParcelableArrayListExtra("data");
    donationId = getIntent().getIntExtra("donationId", 0);

    /* set data to view*/
    name.setText(data.get(donationId).getName());
    donationName.setText(data.get(donationId).getDonationName());
    type.setText(data.get(donationId).getType());

    if(data.get(donationId).getStatus().equals("DITOLAK")){
      status.setTextColor(getResources().getColor(R.color.redFailed));
    }else if(data.get(donationId).getStatus().equals("MENUNGGU PENGIRIMAN")){
      status.setTextColor(getResources().getColor(R.color.blueWaiting));
    }else if(data.get(donationId).getStatus().equals("DITERIMA")){
      status.setTextColor(getResources().getColor(R.color.greenSuccess));
    }
    status.setText(data.get(donationId).getStatus());

    Glide.with(this).load(data.get(donationId).getUrlPicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(img);
  }

  @Override
  public void onClick(View view) {
    if(view.getId() == R.id.btn_finish_detail_donation){
      finish();
    }
  }
}
