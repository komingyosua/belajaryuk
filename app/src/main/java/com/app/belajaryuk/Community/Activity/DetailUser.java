package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.belajaryuk.Data.User;
import com.app.belajaryuk.R;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

public class DetailUser extends AppCompatActivity implements View.OnClickListener {
  ImageView avatar;
  TextView name, event;
  Button acc, decline, already;
  private int userId;
  private List<User> data = new ArrayList<>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_user);

    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    /* resource initialization */
    avatar = findViewById(R.id.user_detail_picture);
    name = findViewById(R.id.user_detail_name);
    event = findViewById(R.id.user_detail_event);
    acc = findViewById(R.id.btn_user_accept);
    decline = findViewById(R.id.btn_user_decline);
    already = findViewById(R.id.btn_disabled_detail_user);
    acc.setOnClickListener(this);
    decline.setOnClickListener(this);

    /* get data from intent */
    userId = getIntent().getIntExtra("userId",0);
    data = getIntent().getParcelableArrayListExtra("data");

    /* button setting */
    if(data.get(userId).getStatus().equals("process")){
      acc.setVisibility(View.VISIBLE);
      decline.setVisibility(View.VISIBLE);
      already.setVisibility(View.GONE);
    }else if(data.get(userId).getStatus().equals("accepted")){
      already.setVisibility(View.VISIBLE);
      already.setText("User telah diterima");
    }else if(data.get(userId).getStatus().equals("declined")){
      already.setVisibility(View.VISIBLE);
      already.setText("User telah ditolak");
    }

    /* put intent data to view*/
    name.setText(data.get(userId).getName());
    event.setText(data.get(userId).getEvent());
    Glide.with(this).load(data.get(userId).getUrlProfilePicture()).fitCenter().diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(true).into(avatar);


  }

  @Override
  public void onClick(View view) {
    switch (view.getId()){
      case R.id.btn_user_accept:

        Toast.makeText(this, "User telah diterima!", Toast.LENGTH_LONG).show();
        finish();
        break;
      case R.id.btn_user_decline:
        // TODO: User Decline Logic
        Toast.makeText(this, "User telah ditolak!", Toast.LENGTH_LONG).show();
        finish();
        break;
    }
  }
}
