package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.app.belajaryuk.Adapter.DonationAdapterCommunity;
import com.app.belajaryuk.Data.Donation;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.DonationDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonationCommunity extends AppCompatActivity implements View.OnClickListener{

  private static final String TAG = DonationCommunity.class.getSimpleName();
  RecyclerView rv_donation;
  LinearLayout not_found, btmButton;
  Button createDonation, createDonationForm;
  private String token;
  private ArrayList<Donation> data;
  private DonationAdapterCommunity donationAdapterCommunity;
  private DonationDataService service;
  SharedPrefManager sharedPrefManager;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_donation_community);

    sharedPrefManager = new SharedPrefManager(this);
    token = sharedPrefManager.getSPToken();
    Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
    service = ServiceGenerator.getClient().create(DonationDataService.class);

    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    /* Resource Initialization */
    rv_donation = findViewById(R.id.rv_donation_community);
    not_found = findViewById(R.id.donation_empty_community);
    btmButton = findViewById(R.id.btn_bottom_create_donation_community);
    createDonation = findViewById(R.id.btn_create_donation_community);
    createDonationForm = findViewById(R.id.btn_createform_donation_community);
    createDonation.setOnClickListener(this);
    createDonationForm.setOnClickListener(this);

    loadData();
    donationAdapterCommunity = new DonationAdapterCommunity(this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      rv_donation.setLayoutManager(layoutManager);
      rv_donation.setAdapter(donationAdapterCommunity);


  }

  @Override
  public void onClick(View view) {
    if(view.getId() == R.id.btn_create_donation_community || view.getId() == R.id.btn_createform_donation_community){
      Intent donation = new Intent(this, FormDonationCommunity.class);
      donation.putExtra("type", "create");
      startActivity(donation);
    }

  }

  private void loadData() {
    Call<BaseResponse<List<Donation>>> call = service.apiRead(token);
    call.enqueue(new Callback<BaseResponse<List<Donation>>>() {
      @Override
      public void onResponse(Call<BaseResponse<List<Donation>>> call, Response<BaseResponse<List<Donation>>> response) {
        if (response.code() == 200) {
          if (response.body().getData() != null) {
            Log.d("DATA : ", response.body().getData().toString());
            donationAdapterCommunity.addAll(response.body().getData());
          } else {
            Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
            Log.d("MESSAGE : ", response.body().getMessage());
          }
        }
      }

      @Override
      public void onFailure(Call<BaseResponse<List<Donation>>> call, Throwable t) {
        Log.e(TAG+".error", t.toString());
      }
    });
  }


  void addData(){
//    data = new ArrayList<>();
//    data.add(new com.farhan.slider.Data.Donation(0,"100 Buku untuk Anak kurang Mampu","Ayo bantu mereka untuk mendapatkan buku. Supaya mereka juga dapat belajar sepertimu. Berapapun jumlahnya adalah sangat berarti bagi mereka!","https://kelblimbing.malangkota.go.id/wp-content/uploads/sites/79/2016/02/DSC_0370_e.gif","100","Teach For Indonesia","Barang","2019-08-21T14:00:32Z"));
//    data.add(new com.farhan.slider.Data.Donation(
//            1,
//            "Bantu anak tidak mampu",
//            "Ayo bantu mereka",
//            "http://pd.pps.uny.ac.id/sites/pd.pps.uny.ac.id/files/field/image/P_20151030_081230_HDR.jpg",
//            "100","Teach For Indonesia",
//            "Barang",
//            "2019-08-21T14:00:32Z"));

  }

  @Override
  public void onResume() {
    super.onResume();
    donationAdapterCommunity = new DonationAdapterCommunity(this);
    rv_donation.setAdapter(donationAdapterCommunity);
  }
}
