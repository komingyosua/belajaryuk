package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.belajaryuk.Adapter.ArticleHomeAdapter;
import com.app.belajaryuk.Data.Article;
import com.app.belajaryuk.R;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;

public class Community extends AppCompatActivity implements View.OnClickListener {
    FrameLayout activity, donation, volunteer, scanqr;
    LinearLayout ll;
    TextView btnManagement;
    RecyclerView rv_selectedArticle;
    private ArrayList<Article> dataArticle;
    private ArticleHomeAdapter article_community_adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        rv_selectedArticle = findViewById(R.id.rv_article_community);
        activity = findViewById(R.id.btn_activity_community);
        donation = findViewById(R.id.btn_donation_community);
        volunteer = findViewById(R.id.btn_volunteer_community);
        scanqr = findViewById(R.id.btn_scanqr_community);
        ll = findViewById(R.id.btn_profile_community);
        btnManagement = findViewById(R.id.user_management_btn);

        btnManagement.setOnClickListener(this);
        ll.setOnClickListener(this);
        activity.setOnClickListener(this);
        donation.setOnClickListener(this);
        volunteer.setOnClickListener(this);
        scanqr.setOnClickListener(this);


        addDataArticle();
        article_community_adapter = new ArticleHomeAdapter(this, dataArticle);
        RecyclerView.LayoutManager layoutManagerArticle = new LinearLayoutManager(this, RecyclerView.HORIZONTAL,false);
        rv_selectedArticle.setLayoutManager(layoutManagerArticle);
        rv_selectedArticle.setAdapter(article_community_adapter);
        StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void addDataArticle(){
        dataArticle = new ArrayList<>();
        dataArticle.add(new Article(0,"Belajar Matematika Bersama","Buat kamu yang pengen belajar matematikan bareng dengan teman-teman yang memiliki minat yang sama. Ayo segera daftarkan diri untuk mencari wawasan lebih luas.","https://www.indorelawan.org/uploads/gallery/2019-8-13/5d7ba731686ae5e9b0d13767_thumbnail370x250.JPG","2019-08-21T14:00:32Z","Admin"));
        dataArticle.add(new Article(1,"Yuk buat Blog bareng!","Ayo kita belajar buat blog bareng dengan berbagai metode yang dijelaskan secara rinci oleh mentor yang begitu pengalaman di bidangnya. Oleh karena itu kamu tidak akan rugi","https://www.indorelawan.org/uploads/gallery/2019-8-10/5d778e5902583aa3a49da26e_thumbnail370x250.jpg","2019-08-21T14:00:32Z","Admin"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_activity_community:
                Intent activity = new Intent(this, AktivitasCommunity.class);
                startActivity(activity);
                break;
            case R.id.btn_volunteer_community:
                Intent volunteer = new Intent(this, RelawanCommunity.class);
                startActivity(volunteer);
                break;
            case R.id.btn_profile_community:
                Intent profile = new Intent(this, ProfileCommunity.class);
                startActivity(profile);
                break;
            case R.id.user_management_btn:
                Intent management = new Intent(this, UserManagement.class);
                startActivity(management);
                break;
            case R.id.btn_scanqr_community:
                Intent qr = new Intent(this, ScanQR.class);
                startActivity(qr);
                break;
            case R.id.btn_donation_community:
                Intent donation = new Intent(this, DonationCommunity.class);
                startActivity(donation);
                break;
        }

    }
}
