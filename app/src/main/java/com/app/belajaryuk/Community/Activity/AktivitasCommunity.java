package com.app.belajaryuk.Community.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.app.belajaryuk.Adapter.EventAdapterCommunity;
import com.app.belajaryuk.Data.Event;
import com.app.belajaryuk.Network.Response.BaseResponse;
import com.app.belajaryuk.Network.Service.EventDataService;
import com.app.belajaryuk.Network.ServiceGenerator;
import com.app.belajaryuk.R;
import com.app.belajaryuk.Utils.SharedPrefManager;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AktivitasCommunity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = AktivitasCommunity.class.getSimpleName();
  RecyclerView rv_activity;
  Button createActivity, createActivityBtm;
  LinearLayout not_found,btn_bottom_create;
  private String token;
  private ArrayList<Event> data;
  private EventAdapterCommunity eventAdapter;
  private EventDataService service;
  SharedPrefManager sharedPrefManager;
  private ShimmerFrameLayout mShimmerViewContainer;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_aktivitas_community);

    sharedPrefManager = new SharedPrefManager(this);
    token = sharedPrefManager.getSPToken();
    Log.d("TOKEN EVENT : ", sharedPrefManager.getSPToken());
    service = ServiceGenerator.getClient().create(EventDataService.class);

    /* Statusbar init */
    StatusBarUtil.setColor(this, getResources().getColor(R.color.primaryColor),0);

    /* Resource initialization */
    rv_activity = findViewById(R.id.rv_activity_community);
    not_found = findViewById(R.id.activity_empty_community);
    btn_bottom_create = findViewById(R.id.btn_bottom_create_activity_community);
    createActivity = findViewById(R.id.btn_create_activity_community);
    createActivityBtm = findViewById(R.id.btn_createform_activity_community);
    createActivityBtm.setOnClickListener(this);
    createActivity.setOnClickListener(this);


    /* Recycleview for Activity */

    //if(data.size() > 0){
      loadData();
      eventAdapter = new EventAdapterCommunity(this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      rv_activity.setLayoutManager(layoutManager);
      rv_activity.setAdapter(eventAdapter);
    //}else{
    // not_found.setVisibility(View.VISIBLE);
     // rv_activity.setVisibility(View.GONE);
    //  btn_bottom_create.setVisibility(View.GONE);
   // }
  }

  @Override
  public void onClick(View view) {
      if(view.getId() == R.id.btn_create_activity_community || view.getId() == R.id.btn_createform_activity_community){
        Intent createActivity = new Intent(this, FormEventCommunity.class);
        createActivity.putExtra("type", "create");
        startActivity(createActivity);
      }
  }

  private void loadData() {
    Call<BaseResponse<List<Event>>> call = service.apiRead(token);
    call.enqueue(new Callback<BaseResponse<List<Event>>>() {
      @Override
      public void onResponse(Call<BaseResponse<List<Event>>> call, Response<BaseResponse<List<Event>>> response) {
        if (response.code() == 200) {
          if (response.body().getData() != null) {
            Log.d("DATA : ", response.body().getData().toString());
            eventAdapter.addAll(response.body().getData());
          } else {
            Log.d("DATA : ", "RESPONSE BODY GET DATA NULL!");
            Log.d("MESSAGE : ", response.body().getMessage());
          }
        }
      }

      @Override
      public void onFailure(Call<BaseResponse<List<Event>>> call, Throwable t) {
        Log.e(TAG+".error", t.toString());
      }
    });
  }

  void addDataActivity(){
//    data = new ArrayList<>();
//    data.add(new Event(0, "Belajar Matematika Kelas SMP", "Belajar matematika sambil bermain games-games sehingga suasana belajar menyenangkan tidak seperti di kelas, di sekolah. Hasil yang diharapkan setelah belajar anak – anak bisa menyelesaikan soal matematika dengan mudah.", "Palmerah, Jakarta Barat", "5 Oktober 2019 09:00", "https://www.indorelawan.org/uploads/gallery/2019-8-16/5d7f2d64136a329473937199_thumbnail370x250.jpg"));
//    data.add(new Event(1,"Membuat Blog dan Menulis Artikel","content","Duren Sawit, Jakarta Timur", "15 September 2019 11:00", "https://i0.wp.com/joglosemarnews.com/images/2019/05/290519sd-pk-kottabarat.jpeg?fit=1040%2C780&ssl=1"));
//    data.add(new Event(2,
//     "Belajar Bahasa Inggris",
//     "Belajar bahasa inggris dengan menyenangkan",
//     "Kebon Jeruk, Jakarta Barat",
//     "6 Oktober 2019 09:00",
//     "http://pd.pps.uny.ac.id/sites/pd.pps.uny.ac.id/files/field/image/P_20151030_081230_HDR.jpg"));

  }

  @Override
  public void onResume() {
    super.onResume();
    eventAdapter = new EventAdapterCommunity(this);
    rv_activity.setAdapter(eventAdapter);
  }
}
