package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;

public class Community implements Parcelable {
    private int index;
    private String id;
    private String user_id;
    private String name;
    private String address;
    private String city;
    private String province;
    private String post_code;
    private String phone;
    private String picture;

    public Community(){
        index = GlobalIndex.getInstance().indexCommunity;
        GlobalIndex.getInstance().indexCommunity = GlobalIndex.getInstance().indexCommunity + 1;
    }

    public Community(int index, String id, String user_id, String name, String address, String city, String province, String post_code, String phone, String picture) {
        this.index = index;
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.post_code = post_code;
        this.phone = phone;
        this.picture = picture;
    }

    protected Community(Parcel in) {
        index = in.readInt();
        id = in.readString();
        user_id = in.readString();
        name = in.readString();
        address = in.readString();
        city = in.readString();
        province = in.readString();
        post_code = in.readString();
        phone = in.readString();
        picture = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(province);
        dest.writeString(post_code);
        dest.writeString(phone);
        dest.writeString(picture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Community> CREATOR = new Creator<Community>() {
        @Override
        public Community createFromParcel(Parcel in) {
            return new Community(in);
        }

        @Override
        public Community[] newArray(int size) {
            return new Community[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
