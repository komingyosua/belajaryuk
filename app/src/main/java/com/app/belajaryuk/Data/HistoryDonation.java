package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;

public class HistoryDonation implements Parcelable {
    private int index;
    private String id;
    private String community_id;
    private String title;
    private String desc;
    private String date;
    private String time;
    private String place;
    private String pool;
    private String creator;
    private String category;
    private String picture;
    private String personal_id;
    private String donation_id;
    private String total_given;
    private String send_date;
    private String payment_proof;
    private String status;

    public HistoryDonation(){
        this.index = GlobalIndex.getInstance().indexHistoryDonation;
        GlobalIndex.getInstance().indexHistoryDonation = GlobalIndex.getInstance().indexHistoryDonation + 1;
    }

    public HistoryDonation(int index, String id, String community_id, String title, String desc, String date, String time, String place, String pool, String creator, String category, String picture, String personal_id, String donation_id, String total_given, String send_date, String payment_proof, String status) {
        this.index = index;
        this.id = id;
        this.community_id = community_id;
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.time = time;
        this.place = place;
        this.pool = pool;
        this.creator = creator;
        this.category = category;
        this.picture = picture;
        this.personal_id = personal_id;
        this.donation_id = donation_id;
        this.total_given = total_given;
        this.send_date = send_date;
        this.payment_proof = payment_proof;
        this.status = status;
    }

    protected HistoryDonation(Parcel in) {
        index = in.readInt();
        id = in.readString();
        community_id = in.readString();
        title = in.readString();
        desc = in.readString();
        date = in.readString();
        time = in.readString();
        place = in.readString();
        pool = in.readString();
        creator = in.readString();
        category = in.readString();
        picture = in.readString();
        personal_id = in.readString();
        donation_id = in.readString();
        total_given = in.readString();
        send_date = in.readString();
        payment_proof = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(id);
        dest.writeString(community_id);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(place);
        dest.writeString(pool);
        dest.writeString(creator);
        dest.writeString(category);
        dest.writeString(picture);
        dest.writeString(personal_id);
        dest.writeString(donation_id);
        dest.writeString(total_given);
        dest.writeString(send_date);
        dest.writeString(payment_proof);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HistoryDonation> CREATOR = new Creator<HistoryDonation>() {
        @Override
        public HistoryDonation createFromParcel(Parcel in) {
            return new HistoryDonation(in);
        }

        @Override
        public HistoryDonation[] newArray(int size) {
            return new HistoryDonation[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommunity_id() {
        return community_id;
    }

    public void setCommunity_id(String community_id) {
        this.community_id = community_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPersonal_id() {
        return personal_id;
    }

    public void setPersonal_id(String personal_id) {
        this.personal_id = personal_id;
    }

    public String getDonation_id() {
        return donation_id;
    }

    public void setDonation_id(String donation_id) {
        this.donation_id = donation_id;
    }

    public String getTotal_given() {
        return total_given;
    }

    public void setTotal_given(String total_given) {
        this.total_given = total_given;
    }

    public String getSend_date() {
        return send_date;
    }

    public void setSend_date(String send_date) {
        this.send_date = send_date;
    }

    public String getPayment_proof() {
        return payment_proof;
    }

    public void setPayment_proof(String payment_proof) {
        this.payment_proof = payment_proof;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
