package com.app.belajaryuk.Data;

public class FavoriteVolunteer {
    private String id;
    private int index;
    private String quota;
    private String title;
    private String desc;
    private String date;
    private String place;
    private String picture;
    private String volunteer_id;

    public FavoriteVolunteer(String id, int index, String quota, String title, String desc, String date, String place, String picture, String volunteer_id) {
        this.id = id;
        this.index = index;
        this.quota = quota;
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.place = place;
        this.picture = picture;
        this.volunteer_id = volunteer_id;
    }

    public String getVolunteer_id() {
        return volunteer_id;
    }

    public void setVolunteer_id(String volunteer_id) {
        this.volunteer_id = volunteer_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
