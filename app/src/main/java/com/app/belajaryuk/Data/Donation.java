package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Donation implements Parcelable {
    private int index;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("pool")
    @Expose
    private String pool;
    @SerializedName("creator")
    @Expose
    private String creator;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("place")
    @Expose
    private String place;


    public Donation(){
        this.index = GlobalIndex.getInstance().indexDonation;
        GlobalIndex.getInstance().indexDonation = index + 1;
    }

    public Donation(String id, int index, String title, String desc, String picture, String pool, String place, String creator, String category, String date, String time) {
        this.id = id;
        this.index = index;
        this.title = title;
        this.desc = desc;
        this.picture = picture;
        this.pool = pool;
        this.place = place;
        this.creator = creator;
        this.category = category;
        this.date = date;
        this.time = time;
    }

    protected Donation(Parcel in) {
        id = in.readString();
        index = in.readInt();
        title = in.readString();
        desc = in.readString();
        picture = in.readString();
        pool = in.readString();
        place = in.readString();
        creator = in.readString();
        category = in.readString();
        date = in.readString();
        time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(index);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(picture);
        dest.writeString(pool);
        dest.writeString(place);
        dest.writeString(creator);
        dest.writeString(category);
        dest.writeString(date);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Donation> CREATOR = new Creator<Donation>() {
        @Override
        public Donation createFromParcel(Parcel in) {
            return new Donation(in);
        }

        @Override
        public Donation[] newArray(int size) {
            return new Donation[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

}
