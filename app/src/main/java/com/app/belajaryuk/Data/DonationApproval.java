package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class DonationApproval implements Parcelable {
  private int id;
  private String name;
  private String donationName;
  private String type;
  private String status;
  private String urlPicture;

  public DonationApproval(int id, String name, String donationName, String type, String status, String urlPicture) {
    this.id = id;
    this.name = name;
    this.donationName = donationName;
    this.type = type;
    this.status = status;
    this.urlPicture = urlPicture;
  }

  protected DonationApproval(Parcel in) {
    id = in.readInt();
    name = in.readString();
    donationName = in.readString();
    type = in.readString();
    status = in.readString();
    urlPicture = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(name);
    dest.writeString(donationName);
    dest.writeString(type);
    dest.writeString(status);
    dest.writeString(urlPicture);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<DonationApproval> CREATOR = new Creator<DonationApproval>() {
    @Override
    public DonationApproval createFromParcel(Parcel in) {
      return new DonationApproval(in);
    }

    @Override
    public DonationApproval[] newArray(int size) {
      return new DonationApproval[size];
    }
  };

  public String getUrlPicture() {
    return urlPicture;
  }

  public void setUrlPicture(String urlPicture) {
    this.urlPicture = urlPicture;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDonationName() {
    return donationName;
  }

  public void setDonationName(String donationName) {
    this.donationName = donationName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
