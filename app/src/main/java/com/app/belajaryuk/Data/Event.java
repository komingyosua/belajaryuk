package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event implements Parcelable {
    private int index;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("place")
    @Expose
    private String place;

    @SerializedName("picture")
    @Expose
    private String picture;

    @SerializedName("time")
    @Expose
    private String time;

    public Event(){
        this.index = GlobalIndex.getInstance().indexEvent;
        GlobalIndex.getInstance().indexEvent = index+1;
    }

    public Event(int index, String id, String title, String date, String desc, String place, String picture, String time) {
        this.index = index;
        this.id = id;
        this.title = title;
        this.date = date;
        this.desc = desc;
        this.place = place;
        this.picture = picture;
        this.time = time;
    }


    protected Event(Parcel in) {
        index = in.readInt();
        id = in.readString();
        title = in.readString();
        date = in.readString();
        desc = in.readString();
        place = in.readString();
        picture = in.readString();
        time = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(index);
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(date);
        parcel.writeString(desc);
        parcel.writeString(place);
        parcel.writeString(picture);
        parcel.writeString(time);
    }
}
