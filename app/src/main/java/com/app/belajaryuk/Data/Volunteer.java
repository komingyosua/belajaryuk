package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Volunteer implements Parcelable {
    private int index;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("quota")
    @Expose
    private String quota;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("picture")
    @Expose
    private String picture;

    public Volunteer(){
        this.index = GlobalIndex.getInstance().indexVolunteer;
        GlobalIndex.getInstance().indexVolunteer = index + 1;
    }

    public Volunteer(String id, String quota, String title, String date, String time, String desc, String place, String picture, int index){
        this.id = id;
        this.index = index;
        this.quota = quota;
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.time = time;
        this.place = place;
        this.picture = picture;
    }


    protected Volunteer(Parcel in) {
        id = in.readString();
        quota = in.readString();
        title = in.readString();
        desc = in.readString();
        date = in.readString();
        time = in.readString();
        place = in.readString();
        picture = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(quota);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(place);
        dest.writeString(picture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Volunteer> CREATOR = new Creator<Volunteer>() {
        @Override
        public Volunteer createFromParcel(Parcel in) {
            return new Volunteer(in);
        }

        @Override
        public Volunteer[] newArray(int size) {
            return new Volunteer[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
