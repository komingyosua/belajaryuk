package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class EventDetail implements Parcelable {
    private int index;
    private String id;
    private String title;
    private String date;
    private String desc;
    private String place;
    private String picture;
    private String time;
    private String qr;
    private String personal_id;


    public EventDetail(int index, String id, String title, String date, String desc, String place, String picture, String time, String qr, String personal_id) {
        this.index = index;
        this.id = id;
        this.title = title;
        this.date = date;
        this.desc = desc;
        this.place = place;
        this.picture = picture;
        this.time = time;
        this.qr = qr;
        this.personal_id = personal_id;
    }

    protected EventDetail(Parcel in) {
        index = in.readInt();
        id = in.readString();
        title = in.readString();
        date = in.readString();
        desc = in.readString();
        place = in.readString();
        picture = in.readString();
        time = in.readString();
        qr = in.readString();
        personal_id = in.readString();
    }

    public static final Creator<EventDetail> CREATOR = new Creator<EventDetail>() {
        @Override
        public EventDetail createFromParcel(Parcel in) {
            return new EventDetail(in);
        }

        @Override
        public EventDetail[] newArray(int size) {
            return new EventDetail[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getPersonal_id() {
        return personal_id;
    }

    public void setPersonal_id(String personal_id) {
        this.personal_id = personal_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(index);
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(date);
        parcel.writeString(desc);
        parcel.writeString(place);
        parcel.writeString(picture);
        parcel.writeString(time);
        parcel.writeString(qr);
        parcel.writeString(personal_id);
    }
}
