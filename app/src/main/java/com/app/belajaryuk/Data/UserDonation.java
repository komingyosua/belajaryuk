package com.app.belajaryuk.Data;

import com.google.gson.annotations.SerializedName;

public class UserDonation {
    @SerializedName("id")
    private String id;

    @SerializedName("total_given")
    private int total_given;

    @SerializedName("payment_proof")
    private String payment_proof;

    @SerializedName("status")
    private String status;

    @SerializedName("send_date")
    private String send_date;

    @SerializedName("donation_id")
    private String donation_id;

    public UserDonation(String id, int total_given, String payment_proof, String status, String send_date, String donation_id) {
        this.id = id;
        this.total_given = total_given;
        this.payment_proof = payment_proof;
        this.status = status;
        this.send_date = send_date;
        this.donation_id = donation_id;
    }

    public String getDonation_id() {
        return donation_id;
    }

    public void setDonation_id(String donation_id) {
        this.donation_id = donation_id;
    }

    public String getSend_date() {
        return send_date;
    }

    public void setSend_date(String send_date) {
        this.send_date = send_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotal_given() {
        return total_given;
    }

    public void setTotal_given(int total_given) {
        this.total_given = total_given;
    }

    public String getPayment_proof() {
        return payment_proof;
    }

    public void setPayment_proof(String payment_proof) {
        this.payment_proof = payment_proof;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
