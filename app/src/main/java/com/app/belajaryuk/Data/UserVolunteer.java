package com.app.belajaryuk.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserVolunteer {
    //TODO ALL
    private int index;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("quota")
    @Expose
    private String quota;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("picture")
    @Expose
    private String picture;
}
