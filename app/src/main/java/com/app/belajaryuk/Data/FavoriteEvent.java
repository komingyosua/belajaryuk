package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;

public class FavoriteEvent implements Parcelable {
    private int index;
    private String id;
    private String event_id;
    private String title;
    private String date;
    private String desc;
    private String place;
    private String picture;
    private String time;

    public FavoriteEvent(){
        this.index = GlobalIndex.getInstance().indexFavoriteEvent;
        GlobalIndex.getInstance().indexFavoriteEvent = GlobalIndex.getInstance().indexFavoriteEvent + 1;
    }

    public FavoriteEvent(int index, String id, String event_id, String title, String date, String desc, String place, String picture, String time) {
        this.index = index;
        this.id = id;
        this.event_id = event_id;
        this.title = title;
        this.date = date;
        this.desc = desc;
        this.place = place;
        this.picture = picture;
        this.time = time;
    }

    protected FavoriteEvent(Parcel in) {
        index = in.readInt();
        id = in.readString();
        event_id = in.readString();
        title = in.readString();
        date = in.readString();
        desc = in.readString();
        place = in.readString();
        picture = in.readString();
        time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(id);
        dest.writeString(event_id);
        dest.writeString(title);
        dest.writeString(date);
        dest.writeString(desc);
        dest.writeString(place);
        dest.writeString(picture);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FavoriteEvent> CREATOR = new Creator<FavoriteEvent>() {
        @Override
        public FavoriteEvent createFromParcel(Parcel in) {
            return new FavoriteEvent(in);
        }

        @Override
        public FavoriteEvent[] newArray(int size) {
            return new FavoriteEvent[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
