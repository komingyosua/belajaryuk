package com.app.belajaryuk.Data;

public class FavoriteDonation {
    private String id;
    private int index;
    private String title;
    private String desc;
    private String picture;
    private String pool;
    private String creator;
    private String category;
    private String date;
    private String donation_id;

    public FavoriteDonation(String id, int index, String title, String desc, String picture, String pool, String creator, String category, String date, String donation_id) {
        this.id = id;
        this.index = index;
        this.title = title;
        this.desc = desc;
        this.picture = picture;
        this.pool = pool;
        this.creator = creator;
        this.category = category;
        this.date = date;
        this.donation_id = donation_id;
    }

    public String getDonation_id() {
        return donation_id;
    }

    public void setDonation_id(String donation_id) {
        this.donation_id = donation_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
