package com.app.belajaryuk.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.belajaryuk.Global.GlobalIndex;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Personal implements Parcelable {
    private int index;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("post_code")
    @Expose
    private String post_code;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("bio")
    @Expose
    private String bio;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("picture")
    @Expose
    private String picture;

    public Personal (){
        this.index = GlobalIndex.getInstance().indexPersonal;
        GlobalIndex.getInstance().indexPersonal = GlobalIndex.getInstance().indexPersonal + 1;
    }

    public Personal(int index, String id, String user_id, String name, String address, String city, String province, String post_code, String dob, String gender, String bio, String phone, String picture) {
        this.index = index;
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.post_code = post_code;
        this.dob = dob;
        this.gender = gender;
        this.bio = bio;
        this.phone = phone;
        this.picture = picture;
    }

    protected Personal(Parcel in) {
        index = in.readInt();
        id = in.readString();
        user_id = in.readString();
        name = in.readString();
        address = in.readString();
        city = in.readString();
        province = in.readString();
        post_code = in.readString();
        dob = in.readString();
        gender = in.readString();
        bio = in.readString();
        phone = in.readString();
        picture = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(province);
        dest.writeString(post_code);
        dest.writeString(dob);
        dest.writeString(gender);
        dest.writeString(bio);
        dest.writeString(phone);
        dest.writeString(picture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Personal> CREATOR = new Creator<Personal>() {
        @Override
        public Personal createFromParcel(Parcel in) {
            return new Personal(in);
        }

        @Override
        public Personal[] newArray(int size) {
            return new Personal[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
