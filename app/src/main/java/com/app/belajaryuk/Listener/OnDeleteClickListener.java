package com.app.belajaryuk.Listener;

public interface OnDeleteClickListener {
    void onDeleteClick(int position);
}
